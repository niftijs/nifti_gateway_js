//var nifti_ftelink = require ('/run/media/mmcblk0p2/nifti_gateway_js/nifti_ftelink'); 
var nifti_ftelink = require ('./nifti_ftelink');
var express = require('express'),
    app = express(),
    http = require('http').Server(app),
    WebSocketServer = require('ws').Server,
    wss = new WebSocketServer({
        port: 8090
    });
var bodyParser = require('body-parser');
//console.log ( nifti_ftelink );
var SIMULATION_MODE = 0;
var NORMAL_MODE     = 1;
var mode  =  SIMULATION_MODE ;

/*
// 
/*
* Declaration of the items in the system. 
*/
// NodeArray is an array of nodes that are available in the system. 
var NodeArray = [];
var SystemStateEnum  = {
  IDLE: 1,
  ACTIVE : 2,
  RECORDING: 3,
  SHUTDOWN: 6
};

var SystemState = SystemStateEnum.IDLE;
var sendbytearray = [];
//list of configurations in the system. 
var currentconfig = null;
var systemtime = 0;

var listofnodes = [];
var listofconfigs =[];

var testingindexer = 0;
var CompleteConfiguration = null;

var currentnode = null;
/*
* Declaration of the Connection Status
*/
var ConnectionStateEnum  = {
  IDLE: 1,
  CONNECTED : 2,
};

var SystemConnectionState = ConnectionStateEnum.IDLE;
var SystemTimeStamp = 0;
var c_indx = 0;

var backplane_status = 0;
var tstamp = 0;
var rssi = 0;
var battery = 0;
var ch1_val = 0;
var ch2_val = 0;
var ch3_val = 0;
var uid     = 0;


function normal_startup()
{
    nifti_ftelink.NiftiMainUdpConn = nifti_ftelink.niftiudp_connect(); 
    if ( mode  == NORMAL_MODE )
    {
        nifti_ftelink.pg_connect();
    }
}

normal_startup();
/*
* Declaration of test data for the arrays to send out in raw byte mode
*/
 
app.use(express.static('public'));
 
app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});
 
wss.broadcast = function broadcast(data) {
    wss.clients.forEach(function each(client) {
        client.send(data);
    });
};

/*!
*   \brief ConvertInt32toByteArray
*   \param x The Int32 variable we are converting to a byte array
*/
function ConvertInt32toByteArray(x)
{
    var bytes = new Uint8Array(4);
    var i = 0;
    do {
        bytes[i++] = x & (255);
        x = x>>8;
    } while ( i < 4)
    return bytes;
}

/*!
*   \brief ConvertInt16toByteArray
*   \param x The Int16 variable we are converting to a byte array
*/
function ConvertInt16toByteArray(x)
{
    var bytes = new Uint8Array(2);
    var i = 0;
    do {
        bytes[i++] = x & (255);
        x = x>>8;
    } while ( i < 2 )
    return bytes;
}

/*!
*   \brief AddToArray
*   \param[in] inarray The inarray
*   \param[in] addarray The array of elements we are adding to the inarray
*   \return The array of data we are returning. 
*/
function AddToArray(inarray, addarray)
{
    var length = 0;
    if(inarray == null)
    {
        length = addarray.length
    }
    else
    {
        length = inarray.length + addarray.length;
    }
    var arrout = new Uint8Array(length);
    //copy the inarray into arrout. 
    var i = 0;
    for(i = 0; i < inarray.length; i++)
    {
        arrout[i] = inarray[i];
    }
    for(i = 0; i < addarray.length; i++)
    {
        arrout[i + inarray.length] = addarray[i];
    }
    return arrout;
}


// Send to Everyone
setInterval(function(){
    if(SystemState == SystemStateEnum.RECORDING)
    {
        //the system is running hence we can start sending data out of
        //the required port in the system. 
       
            // SystemTimeStamp += 32;

            var d = new Date();

            // Computing timestamp by reducing UNIX timestamp bytes. This number must be added later.
            var milliseconds = Math.round(d.getTime()) - 1502200000*1000; 
            SystemTimeStamp = milliseconds;

            var bytearray  = [];
            testingindexer = Math.random();
            bytearray = AddToArray(bytearray,ConvertInt32toByteArray(SystemTimeStamp));
            //create a datastream for decoding. 
            var i = 0;
            for(i = 0; i <  currentconfig.activeNodes.length; i++)
            {
                //this is a MobileJazz style node. The assumption is if the node is in the list then a channel on the node is enabled.
                //add node data to the data stream as per the document on ASANA
                var rssi = 50; // data in range [0, 100]
                var battery = 80; // data in range [0, 100]
                var batteryremainingseconds = 3600; //Fixed for MJ. Dynamic on Server. 
                var storageremainingseconds = 1234; //fixed for MJ. Dynamic on Server. 
                var temperature = 240; //24.0 deg C. 

                bytearray = AddToArray(bytearray,ConvertInt16toByteArray(rssi));
                bytearray = AddToArray(bytearray,ConvertInt16toByteArray(battery));
                bytearray = AddToArray(bytearray,ConvertInt32toByteArray(batteryremainingseconds));
                bytearray = AddToArray(bytearray,ConvertInt32toByteArray(storageremainingseconds));
                bytearray = AddToArray(bytearray,ConvertInt16toByteArray(temperature));
                
                if ( mode == NORMAL_MODE )
                {
                    var channelidx;
                             
                    for(channelidx = 0; channelidx <3; channelidx++)
                    {
                        if(currentconfig.activeNodes[i].channels[channelidx].enabled)
                        {
                            if(nifti_ftelink.config.activeNodes[i].channels[channelidx].data.length > 0)
                            {
                                val = nifti_ftelink.config.activeNodes[i].channels[channelidx].data.pop(); // var val = sinewave[sinewave_idx];
                                nifti_ftelink.config.activeNodes[i].channels[0].laststore = val;
                            }
                            else
                            {
                                val = nifti_ftelink.config.activeNodes[i].channels[0].laststore;
                       
                            }
                            //the value of the item / 1000. so 1000 == 1 x
                            bytearray = AddToArray(bytearray,ConvertInt16toByteArray(val));
                            if(val >nifti_ftelink.config.activeNodes[i].channels[channelidx].maxValue )
                            {
                                nifti_ftelink.config.activeNodes[i].channels[channelidx].maxValue = val;
                            }
                            bytearray = AddToArray(bytearray,ConvertInt16toByteArray(nifti_ftelink.config.activeNodes[i].channels[channelidx].maxValue));
                            if(val <nifti_ftelink.config.activeNodes[i].channels[channelidx].minValue )
                            {
                                nifti_ftelink.config.activeNodes[i].channels[channelidx].minValue = val;
                            }
                            bytearray = AddToArray(bytearray,ConvertInt16toByteArray(bytearray,ConvertInt16toByteArray(nifti_ftelink.config.activeNodes[i].channels[channelidx].minValue)));
                            bytearray = AddToArray(bytearray,ConvertInt16toByteArray(0));  // Alarm status
                        }
                    }
                }
                else
                {
                    // Debug values: showing a sinusodial signal for all channels
                    var testValue = Math.sin(SystemTimeStamp/100);
                    var maxValue = 1; // max value not computed, yet
                    var minValue = -1; // min value not computed, yet
                    var alarmStatus = 0; // TOOD: this needs to be an enum
                    //now look at each of the channels
                    //TODO: make this an array parser not a fixed channel (3)
                    if(currentconfig.activeNodes[i].channels[0].enabled)
                    {
                        //the value of the item / 1000. so 1000 == 1
                        bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*testValue));
                        bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*maxValue));
                        bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*minValue));
                        bytearray = AddToArray(bytearray,ConvertInt16toByteArray(alarmStatus));
                    }
                    if(currentconfig.activeNodes[i].channels[1].enabled)
                    {
                        //the value of the item / 1000. so 1000 == 1
                        bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*testValue));
                        bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*maxValue));
                        bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*minValue));
                        bytearray = AddToArray(bytearray,ConvertInt16toByteArray(alarmStatus));
                    }
                    if(currentconfig.activeNodes[i].channels[2].enabled)
                    {
                        //the value of the item / 1000. so 1000 == 1
                        bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*testValue));
                        bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*maxValue));
                        bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*minValue));
                        bytearray = AddToArray(bytearray,ConvertInt16toByteArray(alarmStatus));
                    }
                    
                }
            }    
            wss.broadcast(bytearray);
    }
    else
    {
        if(currentnode != null)
        {
             var bytearray  = [];
             var d = new Date();
            
             // Computing timestamp by reducing UNIX timestamp bytes. This number must be added later.
             var milliseconds = Math.round(d.getTime()) - 1502200000*1000; 
             SystemTimeStamp = milliseconds;
    
             bytearray = AddToArray(bytearray,ConvertInt32toByteArray(SystemTimeStamp));

             var rssi = currentnode.RSSI; // data in range [0, 100]
             var battery = currentnode.Battery; // data in range [0, 100]
             var batteryremainingseconds = currentnode.Storage; 
             var storageremainingseconds = 1200; //storage time remaining in seconds. 
             var temperature = 240; //24.0 deg C. 

             var testValue   = Math.sin(SystemTimeStamp/100);            
             var maxValue    = 1; 
             var minValue    = -1; 
             var alarmStatus = 0; 
             
             bytearray = AddToArray(bytearray,ConvertInt16toByteArray(rssi));
             bytearray = AddToArray(bytearray,ConvertInt16toByteArray(battery));
             bytearray = AddToArray(bytearray,ConvertInt32toByteArray(batteryremainingseconds));
             bytearray = AddToArray(bytearray,ConvertInt32toByteArray(storageremainingseconds));
             bytearray = AddToArray(bytearray,ConvertInt16toByteArray(temperature));

             bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*testValue));
             bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*maxValue));
             bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*minValue));
             bytearray = AddToArray(bytearray,ConvertInt16toByteArray(alarmStatus));
             
             bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*testValue));
             bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*maxValue));
             bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*minValue));
             bytearray = AddToArray(bytearray,ConvertInt16toByteArray(alarmStatus));

             bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*testValue));
             bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*maxValue));
             bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*minValue));
             bytearray = AddToArray(bytearray,ConvertInt16toByteArray(alarmStatus));

             wss.broadcast(bytearray);
        }

    }
}, 32 );
 
// Functions to check WS connection is alive.
function nop() {}

function heartbeat() {
  this.isAlive = true;
}

wss.on('connection', function(ws) {
    ws.isAlive = true;
    ws.on('pong', heartbeat);
    ws.on('message', function(msg) {
        data = JSON.parse(msg);
        if (data.message) wss.broadcast('<strong>' + data.name + '</strong>: ' + data.message);
    });
});


// Shutdown the system if ipad web socket connection is lost
setInterval(function checkWs() {
  var ws_clients = 0;
  if(SystemState == SystemStateEnum.RECORDING)
  {
    ws_clients = 0;
    wss.clients.forEach(function each(ws) {
        console.log ( "web socket status = " + ws.isAlive);
        if (ws.isAlive === false)
        {
            // Send shutdown command
            SystemState = SystemStateEnum.SHUTDOWN;
            if ( mode == NORMAL_MODE )
            {
               nifti_ftelink.shutdown();
            }
        }
        ws.isAlive = false;
        ws.ping (nop);
        ws_clients = ws_clients  + 1;
    });
    if ( ws_clients == 0)
    {
        SystemState = SystemStateEnum.SHUTDOWN;
        if ( mode == NORMAL_MODE )
        {
            nifti_ftelink.shutdown();
        }
    }
  }
},30000);



// Retrying the the user requests till getting a successful response
setInterval( function check_rf_ws() {
    if ( mode == NORMAL_MODE )
    {
        nifti_ftelink.check_niftimain_requests();
    }
},10000 ); // Don`t change the interval time .



// Process niftimian udp message response
nifti_ftelink.NiftiMainUdpConn.on( "message", function( msg, rinfo ) {
    //console.log ( "UDP message received",JSON.stringify(msg));
    if ( mode == NORMAL_MODE )
    {
        nifti_ftelink.process_niftmaster_udpmsg( msg );
    }
});


 
http.listen(1234, function() {
    console.log('listening on *:1234');
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json()); 

/*
* Start OF SECTION APP
*/

app.post('/App/NodeSession/Start', function (request, response) {
    console.log('Starting broadcasting data')
    try
    {
        var node = request.body.nodename;

          var nodedata = new Object();
            nodedata.SerialNumber = node;
            nodedata.Battery = 67;
            nodedata.RSSI = 111;
            nodedata.State = "Active";
            nodedata.Channels = [];
            
            var channel0 = new Object();
            channel0.name = "X";
            channel0.value = 100;

            nodedata.Channels.push(channel0);

             var channel1 = new Object();
            channel1.name = "Y";
            channel1.value = 200;

            nodedata.Channels.push(channel1);

             var channel2 = new Object();
            channel2.name = "Z";
            channel2.value = 300;

            nodedata.Channels.push(channel2);

             var channel3 = new Object();
            channel3.name = "TempCore";
            channel3.value = 22.3;

            nodedata.Channels.push(channel3);

             var channel4 = new Object();
            channel4.name = "TempSkin";
            channel4.value = 12.5;

            nodedata.Channels.push(channel4);

             currentnode = nodedata;
            
        console.log(request.body)

        response.json("{}");
    }
    catch(exception)
    {
        response.json("{\"error\":\"Incorrect Formatted Request\"}");
    }
});

app.post('/App/NodeSession/Stop', function(request, response) {
   console.log('Stopping broadcasting data');
   currentnode = null;
   response.json("{}");
});

/**
 * Start broadcasting the node data through the sockets.
 */
app.post('/App/Session/Start', function (request, response) {
    var taskId = request.params.id;
    try {
        //We have a nodearray. If it is empty then we don't go on. 
        if(listofconfigs == null)
        {
            response.json("{\"error\":\"No Configuration Loaded in system or configurations\"}");
            return;
        }
        //check that the configuration in this command actually exists. 
        var startdata = request.body;
         console.log(request.body);
        //get the configuration from startdata.
        if(startdata.configuration == null)
        {
              response.json("{\"error\":\"Configuration FIELD does not exist in the param data\"}");
                return;
        }

        //get the string and try to find it in the configuration data. 
       var i = 0;
       var found = -1;
       for(i  =0; i < listofconfigs.length; i++)
       {
           if(listofconfigs[i].identifier == startdata.configuration)
           {
               found = i;
           }
       }
       if(found < 0)
       {
             response.json("{\"error\":\"Configuration " +startdata.configuration + " not found in the configuration file on the device\"}");
             return;
       }
       SystemTimeStamp = 0;
       currentconfig = listofconfigs[found];
       if ( mode == NORMAL_MODE )
       {
           nifti_ftelink.start(currentconfig);
       }
       SystemState = SystemStateEnum.ACTIVE;
       response.json("{\"configuration\":\""+startdata.configuration +"\"}");
    }
    catch (exeception) {
        response.status(404).send("No Valid Configuration Loaded in System");
    }
});

/**
 * Stop broadcasting the node data through the sockets.
 */
app.post('/App/Session/Stop', function (request, response) {
    var taskId = request.params.id;
   
    // IDLE MODE
    if ( mode == NORMAL_MODE )
    {
        nifti_ftelink.stop();
    }
    try {
            //TODO: Put in the logic for RECORDING, If we are recording then do a stop recording THEN a STOP command. 
       if(SystemState != SystemStateEnum.ACTIVE)
        {
           //Error Response 405: Not Acceptable. This function is not acceptable if the system is currently running. 
           response.send(406);
           return;
        }
        else
        {
            var timestamp = request.body;
             console.log(request.body);
            if(timestamp == null)
            {
                response.json("{\"error\":\"Null Body\"}");
                 return;
            }
            else if(timestamp.time == null)
            {
                 response.json("{\"error\":\"Invalid Time (Does it Exist?)\"}");

            }
            else if(timestamp.time >SystemTimeStamp)
            {
                 response.json("{\"error\":\"Invalid Time (Occurs in the Future)\"}");
            }
            else
            {
                addEventStop()
                SystemState = SystemStateEnum.IDLE;
                response.json("{\"state\":\"IDLE\"}");
            }
        }
    } catch (exeception) {
        response.send(404);
    }
});

/**
 * /App/Session/Command/StartRecording
 */
app.post('/App/Session/Command/StartRecording', function(request, response) {
    SystemState = SystemStateEnum.RECORDING;
    if ( mode == NORMAL_MODE )
    {
        nifti_ftelink.start_recording();
    }
    response.json("{}");
});

/**
 * /App/Session/Command/StopRecording
 */
app.post('/App/Session/Command/StopRecording', function(request, response) {
    SystemState = SystemStateEnum.ACTIVE;
    if ( mode == NORMAL_MODE )
    {
        nifti_ftelink.stop_recording();
    }
    response.json("{}");
});

/*  /App/Session/Command/TimeStamp
*
*   A timestamp is a function that records an action in the system. It has the following format
*   {"time":TimestampTime,
*     "event":"This is a string"}
*
*   time: time is the time based on the time that START was pressed. This is the sync time in the system. 
*   event: event is the event that has occured in the system. This is a string to be inserted into the NIFTI SubSystem. 
*/
app.post('/App/Session/Command/TimeStamp', function(request, response) {
    if ( mode == NORMAL_MODE )
    {
        nifti_ftelink.timestamp();
    }
    try {
        if( SystemState != SystemStateEnum.RECORDING)
        {
          //Error Response 405: Not Acceptible. This function is not acceptable if the system is currently running. 
           response.send(406);
           return;
        }
        else
        {
             var timestamp = request.body;

            //lets split the JSON data out of the request. Should this be from the IPAD Application or from the rolling timer in the system
            //as they are not perfectly in SYNC and never will be. 
            addEventTimestamp(timestamp);

            response.json(request.body)
        }
    } catch (exeception) {
        response.send(404);
    }
});

/**
 * /section CONFIGURATION
 */
app.get('/App/Session/Configuration', function (request, response) {
    var taskId = request.params.id;
      try {
          response.json(CompleteConfiguration);
      } catch (exeception) {
          response.send(404);
      }
  });
  
  /*!
  * /App/Session/Configuration
  * Sending a new configuration to the iPad.
  */
  app.post('/App/Session/Configuration', function (request, response) {
    var taskId = request.params.id;
      try {
          //lets decode the app/configuration data. 
          console.log(request.body);
          var task = request.body;
  
          //check the configuration for completeness. 
          if(task.task.identifier == null)
          {
              response.json("{\"error\":\"Invalid Configuration (No Name)\"}");
              return;
          }
          //we have a name. DO we have a previously loaded configuration?
          if(currentconfig != null)
          {
              //TODO: How to determine if we want this new configuration file. Currently we just accept it and move on. 
          }
          //get the list of nodes in the system. 
  
          var aircraftsetup = task.task.aircraftsetup;
  
          if(aircraftsetup.identifier == null)
          {
              response.json("{\"error\":\"aircraft-Setup parsing error\"}");
              return;
          }
              
          //MobileJazz Version has NO nodelist. Just Configurations and active Nodes. 
          listofconfigs = aircraftsetup.configurations;
                           
          if(listofconfigs == null)
          {
              response.json("{\"error\":\"List of configurations is empty or invalid\"}");
              return;
          }
          else if(listofconfigs.length == 0)
          {
              response.json("{\"error\":\"List of configurations is empty\"}");
              return;
          }
          //Cannot validate list of nodes against configuration as there is NO list of nodes. 
          CompleteConfiguration = task;
          response.json("{}");
      } catch (exeception) {
          response.send(404);
      }
  });
  
  /*
  * END OF SECTION CONFIGURATION
  */

/*
  * Start OF SECTION APP
  */

  /*  /App/setDateTime
*
*  Allows the Application to set the Date Time of the Gateway Via the application so that the events that occur on the application are linked to the events
* that occur on the gateway
*/
app.post('/App/setDateTime', function (request, response) {
    try {
        // function to set datetime needs to go here. 
        if ( mode == NORMAL_MODE )
        {
           //TODO: Add date time setting. 
        }
        response.sendStatus(200);
     } catch (exeception) {
         //we should never get here
         response.send(406);
     }
 });

 /*
* \Starts Node Discovery on the gateway which will then start updating the MASTER endpoint with all of the nodes that it finds. 
*/
app.get('/App/startNodeDiscovery', function (request, response) {
    var taskId = request.params.id;
      try {
        if ( mode == NORMAL_MODE )
        {
            //nifti_ftelink.timestamp(); TODO: Add endpoint in nifti_ftelink to start node discovery.
        }
          response.json("{}");
          console.log(request.body);
      } catch (exeception) {
          response.send(404);
      }
  });

   /*
*  Stops Node Discovery on the gateway. 
*/
app.get('/App/stopNodeDiscovery', function (request, response) {
    var taskId = request.params.id;
      try {
        if ( mode == NORMAL_MODE )
        {
            //nifti_ftelink.timestamp(); TODO: Add endpoint in nifti_ftelink to STOP node discovery.
        }
          response.json("{}");
          console.log(request.body);
      } catch (exeception) {
          response.send(404);
      }
  });

/*  /App/Shutdown
*
*  Shutdown will completely shutdown the system, shutdown the gateway and all nodes. You will loose connection to the gateway after this command
*   The command requires NO Parameters. 
*/
app.post('/App/Shutdown', function (request, response) {
   if ( mode == NORMAL_MODE )
   {
       nifti_ftelink.shutdown();
   }
   try {
        SystemState = SystemStateEnum.SHUTDOWN;
        response.json("{\"state\":\"shutdown\"}");
        var timestamp = request.body;
        //timestamp can be null, this is emergency shutdown. 
        addEventShutdown(timestamp);
    } catch (exeception) {
        //we should never get here
        response.send(406);
    }
});

/* /App/SystemOff
*
* The system off command is used to turn off the nodes and hibernate the system. This is not called hibernate due to the confusion that Start / Stop, hibernate, wake has caused hence
* Hibernate and Wake have been renamed SystemOff and SystemOn. It requires no parameters. 
*/
app.post('/App/SystemOff', function (request, response) {
  var val = "SYSTEM OFF\n";
  if ( mode == NORMAL_MODE )
  {
      nifti_ftelink.logNiftiEvents ( val );
  }
    try {
        //get the configuration from start data.
         SystemState = SystemStateEnum.IDLE;
         response.json({"state": "systemoff"});
    } catch (exeception) {
        response.sendStatus(503);
    }
});

/* /App/SystemOn
*
* The system on command is used to turn on the nodes. This is not called wake due to the confusion that Start / Stop, hibernate, wake has caused hence
* Hibernate and Wake have been renamed SystemOff and SystemOn. It requires no parameters. SystemOn will wake ALL Nodes in the system and send their
* data to the IPAD application at a slow rate. It is used for determining if the nodes have been installed correctly and to get node data (battery level, rssi, etc)
*/
app.post('/App/SystemOn', function (request, response) {
  var val = "SYSTEM ON\n";
  if ( mode == NORMAL_MODE )
  {
      nifti_ftelink.logNiftiEvents ( val );
  }
  var taskId = request.params.id;
    try {
        var startdata = request.body;
         console.log(request.body);
        //get the configuration from startdata.
         SystemState = SystemStateEnum.ACTIVE;
         response.json("{\"state\":\"systemon\"}");
    } catch (exeception) {
       
    }
});

/* /App/TimeStamp
*
* The timestamp command is used to return the timestamp in the system. 
*/
app.get('/App/TimeStamp', function (request, response) {
      var taskId = request.params.id;
      try {
          if( SystemState != SystemStateEnum.IDLE)
          {
              response.json("{\"error\":\"System is not Running\"}");
              console.log(request.body);
          }
          else
          {
              var timestampresponse = {
                  timestamp:SystemTimeStamp
              };
  
              var timestamp = JSON.stringify(timestampresponse);
              response.json(timestamp);
          }
      } catch (exeception) {
          response.send(404);
      }
  });  

/**
 * END OF SECTION APP
 */

/**
 * /section NIFTINODES
 */

var downloadState = {
    step: 0,
    nodes: 4,
    recordsPerNode: 10,
    reset() {
        this.step = 0;
    },
    status() {
        console.log('step: ', this.step);
        const time = 15699874570;
        var string = '';
        var it = 0;
        for (let i=0; i<this.nodes; ++i) {
            for (let j=0; j<this.recordsPerNode; ++j) {
                if (it > this.step) {
                    break;
                }
                string = string.concat(`${(time + (it*11))}: "A${i}" record ${j*100} [${i} of ${this.nodes} nodes]\n`);
                if (j === this.recordsPerNode-1) {
                    string = string.concat(`${(time + (it*11))}: Download complete for node "A${i}" at record ${this.recordsPerNode*100} [${i} of ${this.nodes} nodes]\n`);
                }
                it = it + 1;
            }
        }
        if (it === this.nodes*this.recordsPerNode) {
            string = string.concat('Download completed\n');
        }
        this.step = this.step + 1;
        return string;
    }
};

app.get('/App/Session/Command/StartDownload', function (request, response) {
    downloadState.reset();
    response.send(200);
});

app.get('/App/Session/Command/DownloadStatus', function (request, response) {
    const text = downloadState.status();
    console.log('text: ', text);
    response.send(text);
});

app.get('/App/Session/Command/StopDownload', function (request, response) {
    downloadState.reset();
    response.send(200);
});

app.get('/App/NiftiNodes', function (request, response) {
  var val = "APP NIFTINODES\n";
  if ( mode == NORMAL_MODE )
  {
      nifti_ftelink.logNiftiEvents ( val );
  }
  var taskId = request.params.id;
    try {
        response.json("{}");
    } catch (exeception) {
        response.send(404);
    }
});

/*
* END OF SECTION NIFTINODES
*/

/**
 * /section MASTER
 */
app.get('/Slave', function (request, response) {
   var task = request.body;
    var taskId = request.params.id;
    try {
        response.json("{\"slavelist\":\"Where are the slaves?\"}");
    } catch (exeception) {
        response.send(404);
    }
});

app.get('/Slave/:id', function (request, response) {
   var task = request.body;
    var taskId = request.params.id;
    try {
        response.json("{}");
    } catch (exeception) {
        response.send(404);
    }
});


app.get('/Master/Node/:id', function (request, response) {
  var task = request.body;
    var taskId = request.params.id;
    try {
            var nodedata = new Object();
            nodedata.SerialNumber = request.params.id
            nodedata.Battery = 67;
            nodedata.RSSI = 111;
            nodedata.State = "Active";
            nodedata.Channels = [];
            
            var channel0 = new Object();
            channel0.name = "X";
            channel0.value = 100;

            nodedata.Channels.push(channel0);

             var channel1 = new Object();
            channel1.name = "Y";
            channel1.value = 200;

            nodedata.Channels.push(channel1);

             var channel2 = new Object();
            channel2.name = "Z";
            channel2.value = 300;

            nodedata.Channels.push(channel2);

             var channel3 = new Object();
            channel3.name = "TempCore";
            channel3.value = 22.3;

            nodedata.Channels.push(channel3);

             var channel4 = new Object();
            channel4.name = "TempSkin";
            channel4.value = 12.5;

            nodedata.Channels.push(channel4);



        switch(request.params.id)
        {
            case "17":
                 currentnode = nodedata;
                 response.json(JSON.stringify(nodedata));
                 break;
            case "18":
                 currentnode = nodedata;
                 response.json(JSON.stringify(nodedata));
                 break;
            case "33":
                 currentnode = nodedata;
                 response.json(JSON.stringify(nodedata));
                 break;
            case "34":
                 currentnode = nodedata;
                 response.json(JSON.stringify(nodedata));
                 break;
            case "49":
                 currentnode = nodedata;
                 response.json(JSON.stringify(nodedata));
                 break;
            case "50":
                 currentnode = nodedata;
                 response.json(JSON.stringify(nodedata));
                 break;
            
            default:
                response.json("{\"error\":\"node not found\"}");
            break;

        }
        
    } catch (exeception) {
        response.send(404);
    }
});



/* /Master
*
* Returns a massive list of all of the items and objects that are in the NIFTI system. The objects are listed in the example below
*/
app.get('/Master', function (request, response) {
  var taskId = request.params.id;
    try {
        var startdata = request.body;
         console.log(request.body);

          /* The following has been done in an object style to allow for easier manipulation of the objects */
          if ( mode == NORMAL_MODE )
          {
              nifti_ftelink.logNiftiEvents ( val );
          }
          else
          {
                var gateway = new Object();
                gateway.SerialNumber = "112233445566778899AABBCCDDEEFF00";
                gateway.ProductID = "NGW-03";
                gateway.HWVersion = "0.3";
                gateway.SWVersion = "0.2.2"
                gateway.Status = SystemState; //Idle, Active, Recordinging, Shutdown
                gateway.slaves = [];
                //add storage type to gateway. 

                var gateway_storage0 = new Object();
                gateway_storage0.CardDetected = 1;
                gateway_storage0.Capacity = 1 * 1024 * 1024 * 1024; //1 GB. 
                gateway_storage0.Usage = 0.25 * 1024 * 1024 * 1024; //256 MB. 

                var gateway_storage1 = new Object();
                gateway_storage1.CardDetected = 1;
                gateway_storage1.Capacity = 1 * 1024 * 1024 * 1024; //1 GB. 
                gateway_storage1.Usage = 0.45 * 1024 * 1024 * 1024; //256 MB. 

                gateway.Storage = [];
                gateway.Storage.push(gateway_storage0);
                gateway.Storage.push(gateway_storage1);

                var slave_ngw02= new Object();
                var slave_ngw02_powersource= new Object();

                slave_ngw02.SerialNumber = "DEADBEEF000000000000000000000012";
                slave_ngw02.ProductID = "NGW-02";
                slave_ngw02.HWVersion = "0.3";
                slave_ngw02.SWVersion = "0.2.2"
                // slave_ngw02.PowerSource = slave_ngw02_powersource

                var slave_ngw02_powerin = new Object();
                slave_ngw02.PowerIn = slave_ngw02_powerin;
                slave_ngw02_powerin.VoltageIn = 24.0;   //in Volts
                slave_ngw02_powerin.CurrentDraw = 1.56; //in Amps
                slave_ngw02_powerin.OnBattery = true; //Is the system running on battery power.    


                // slave_ngw02_powersource.VoltageIn = 28.3;
                // slave_ngw02_powersource.CurrentIn = 0.800;
                // slave_ngw02_powersource.VoltageOut3V3 = 3.298;
                // slave_ngw02_powersource.CurrentOut3V3 = 0.650;
                // slave_ngw02_powersource.VoltageOut5V0 = 5.015;
                // slave_ngw02_powersource.CurrentOut5V0 = 0.200;
                // slave_ngw02_powersource.temperature = 38.5;

                gateway.slaves.push(slave_ngw02);

                ///// GPS MODULE 

                var slave_ngw04= new Object();
                var slave_ngw04_powersource= new Object();
                var slave_ngw04_gps = new Object();

                slave_ngw04.SerialNumber = "DEADBEEF000000000000000000000012";
                slave_ngw04.ProductID = "NGW-04";
                slave_ngw04.HWVersion = "0.1";
                slave_ngw04.SWVersion = "10.2"
                //slave_ngw04.PowerSource = slave_ngw04_powersource;
                slave_ngw04.gps = slave_ngw04_gps;

                //slave_ngw04_powersource.VoltageIn = 28.3;
                //slave_ngw04_powersource.CurrentIn = 0.800;
                //slave_ngw04_powersource.VoltageOut3V3 = 3.298;
                //slave_ngw04_powersource.CurrentOut3V3 = 0.650;
                //slave_ngw04_powersource.VoltageOut5V0 = 5.015;
                //slave_ngw04_powersource.CurrentOut5V0 = 0.200;
                //slave_ngw04_powersource.temperature = 38.5;

                slave_ngw04_gps.lock = true;
                slave_ngw04_gps.position = "4807.038,N01131.000,E";
                slave_ngw04_gps.clockout = "1000";
                slave_ngw04_gps.satellites = 8;
                slave_ngw04_gps.altitude = 848;

                gateway.slaves.push(slave_ngw04);


                ///// IMU MODULE 

                var slave_ngw07= new Object();
                var slave_ngw07_powersource= new Object();
                var slave_ngw07_imu = new Object();

                slave_ngw07.SerialNumber = "DEADBEEF000000000000000000000012";
                slave_ngw07.ProductID = "NGW-07";
                slave_ngw07.HWVersion = "0.1";
                slave_ngw07.SWVersion = "10.2"
                //slave_ngw07.PowerSource = slave_ngw07_powersource;
                slave_ngw07.imu = slave_ngw07_imu;

                //slave_ngw07_powersource.VoltageIn = 28.3;
                //slave_ngw07_powersource.CurrentIn = 0.800;
                //slave_ngw07_powersource.VoltageOut3V3 = 3.298;
                //slave_ngw07_powersource.CurrentOut3V3 = 0.650;
                //slave_ngw07_powersource.VoltageOut5V0 = 5.015;
                //slave_ngw07_powersource.CurrentOut5V0 = 0.200;
                //slave_ngw07_powersource.temperature = 38.5;

                slave_ngw07_imu.roll = 0.20;
                slave_ngw07_imu.pitch = -20.00;
                slave_ngw07_imu.yaw = 5.30;

                gateway.slaves.push(slave_ngw07);

                ///// IRIG MODULE 
                var slave_ngw08= new Object();
                var slave_ngw08_powersource= new Object();
                var slave_ngw08_irig = new Object();
                
                slave_ngw08.SerialNumber = "DEADBEEF000000000000000000000012";
                slave_ngw08.ProductID = "NGW-07";
                slave_ngw08.HWVersion = "0.1";
                slave_ngw08.SWVersion = "10.2"
                //slave_ngw08.PowerSource = slave_ngw08_powersource;
                slave_ngw08.irig = slave_ngw08_irig;
                
                //slave_ngw08_powersource.VoltageIn = 28.3;
                //slave_ngw08_powersource.CurrentIn = 0.800;
                //slave_ngw08_powersource.VoltageOut3V3 = 3.298;
                //slave_ngw08_powersource.CurrentOut3V3 = 0.650;
                //slave_ngw08_powersource.VoltageOut5V0 = 5.015;
                //slave_ngw08_powersource.CurrentOut5V0 = 0.200;
                //slave_ngw08_powersource.temperature = 38.5;
                
                slave_ngw08_irig.packettype = "PWM Type 1";
                slave_ngw08_irig.port = 1000;
                slave_ngw08_irig.ip = "192.168.16.100";
                slave_ngw08_irig.blocksize = 8;
                slave_ngw08_irig.blocktype = "uint16";
                slave_ngw08_irig.dataarrray = [];
                //there are currently slave_ngw08_irig.blocksize in the data array. 
                //This needs to be a link to data on a node /channel or Module / channel
                slave_ngw08_irig.dataarrray.push(1000);
                slave_ngw08_irig.dataarrray.push(1000);
                slave_ngw08_irig.dataarrray.push(1000);
                slave_ngw08_irig.dataarrray.push(1000);
                slave_ngw08_irig.dataarrray.push(1000);
                slave_ngw08_irig.dataarrray.push(1000);
                slave_ngw08_irig.dataarrray.push(1000);
                slave_ngw08_irig.dataarrray.push(1000);
                
                gateway.slaves.push(slave_ngw08);

                //Battery Module

                var slave_ngw09= new Object();
                var slave_ngw09_powerstore= new Object();
                var slave_ngw09_powerin= new Object();

                slave_ngw09.SerialNumber = "80081E50000000000000000000000FF";
                slave_ngw09.ProductID = "NGW-09";
                slave_ngw09.HWVersion = "0.2";
                slave_ngw09.SWVersion = "0.1.2"
                slave_ngw09.PowerStore = slave_ngw09_powerstore;
                //slave_ngw09.PowerIn = slave_ngw09_powerin;

                //slave_ngw09_powerin.VoltageOut3V3 = 3.298;
                //slave_ngw09_powerin.CurrentOut3V3 = 0.650;
                //slave_ngw09_powerin.VoltageOut5V0 = 5.015;
                //slave_ngw09_powerin.CurrentOut5V0 = 0.200;
                //slave_ngw09_powerin.temperature = 38.5;

                slave_ngw09_powerstore.MaxCapacity = "180.00";
                slave_ngw09_powerstore.CurrentCapacity = "178.00";
                slave_ngw09_powerstore.Temperature = "44.5";
                gateway.slaves.push(slave_ngw09);


                var slave_ngw05= new Object();
                var slave_ngw05_powerin= new Object();
                var slave_ngw05_node0= new Object();
                var slave_ngw05_node1= new Object();
                var slave_ngw05_node2= new Object();
                var slave_ngw05_node3= new Object();
                var slave_ngw05_node4= new Object();
                var slave_ngw05_node5= new Object();

                slave_ngw05.SerialNumber = "80081E50000000000000000000000FF";
                slave_ngw05.ProductID = "NGW-05";
                slave_ngw05.HWVersion = "0.2";
                slave_ngw05.SWVersion = "0.1.2"
                slave_ngw05.PowerIn = slave_ngw05_powerin;

                //slave_ngw05_powerin.VoltageOut3V3 = 3.298;
                //slave_ngw05_powerin.CurrentOut3V3 = 0.650;
                //slave_ngw05_powerin.VoltageOut5V0 = 5.015;
                //slave_ngw05_powerin.CurrentOut5V0 = 0.200;
                //slave_ngw05_powerin.temperature = 38.5;


                slave_ngw05.NiftiLink = new Object()
                slave_ngw05.NiftiLink.NodeList = [];
                
                slave_ngw05_node0.SerialNumber = "17";
                slave_ngw05_node0.Battery = 100;
                slave_ngw05_node0.Storage = 100;
                slave_ngw05_node0.RSSI = 75;
                slave_ngw05_node0.batteryremainingseconds = 2400;
                slave_ngw05_node0.State = "Active"; //Idle == sleep, Active == awake. 
                slave_ngw05_node0.TransmissionRate = 0;
                slave_ngw05_node0.SampleRate = 0;
                slave_ngw05_node0.Firmware = 1000;

                slave_ngw05_node1.SerialNumber = "18";
                slave_ngw05_node1.Battery = 100;
                slave_ngw05_node1.Storage = 100;
                slave_ngw05_node1.RSSI = 75;
                slave_ngw05_node1.batteryremainingseconds = 3600;
                slave_ngw05_node1.State = "Active";
                slave_ngw05_node1.TransmissionRate = 0;
                slave_ngw05_node1.SampleRate = 0;
                slave_ngw05_node1.Firmware = 1001;

                slave_ngw05_node2.SerialNumber = "33";
                slave_ngw05_node2.Battery = 100;
                slave_ngw05_node2.Storage = 100;
                slave_ngw05_node2.RSSI = 75;
                slave_ngw05_node2.batteryremainingseconds = 3500;
                slave_ngw05_node2.State = "Active";
                slave_ngw05_node2.TransmissionRate = 0;
                slave_ngw05_node2.SampleRate = 0;
                slave_ngw05_node2.Firmware = 1002;
                
                
                slave_ngw05_node3.SerialNumber = "34";
                slave_ngw05_node3.Battery = 100;
                slave_ngw05_node3.Storage = 100;
                slave_ngw05_node3.RSSI = 75;
                slave_ngw05_node3.batteryremainingseconds = 3601;
                slave_ngw05_node3.State = "Active";
                slave_ngw05_node3.TransmissionRate = 0;
                slave_ngw05_node3.SampleRate = 0;
                slave_ngw05_node3.Firmware = 1003;

                slave_ngw05_node4.SerialNumber = "49";
                slave_ngw05_node4.Battery = 100;
                slave_ngw05_node4.Storage = 100;
                slave_ngw05_node4.RSSI = 75;
                slave_ngw05_node4.batteryremainingseconds = 3200;
                slave_ngw05_node4.State = "Active";
                slave_ngw05_node4.TransmissionRate = 0;
                slave_ngw05_node4.SampleRate = 0;
                slave_ngw05_node4.Firmware = 1004;
                
                slave_ngw05_node5.SerialNumber = "50";
                slave_ngw05_node5.Battery = 100;
                slave_ngw05_node5.Storage = 100;
                slave_ngw05_node5.RSSI = 75;
                slave_ngw05_node5.batteryremainingseconds = 2800;
                slave_ngw05_node5.State = "Active";
                slave_ngw05_node5.TransmissionRate = 0;
                slave_ngw05_node5.SampleRate = 0;
                slave_ngw05_node5.Firmware = 1005;

                slave_ngw05.NiftiLink.NodeList.push(slave_ngw05_node0);
                slave_ngw05.NiftiLink.NodeList.push(slave_ngw05_node1);
                slave_ngw05.NiftiLink.NodeList.push(slave_ngw05_node2);
                slave_ngw05.NiftiLink.NodeList.push(slave_ngw05_node3);
                slave_ngw05.NiftiLink.NodeList.push(slave_ngw05_node4);
                slave_ngw05.NiftiLink.NodeList.push(slave_ngw05_node5);
                
                gateway.slaves.push(slave_ngw05);
                //get the configuration from start data.

                response.json(gateway);
        }
    } catch (exeception) {
         response.json(JSON.stringify(exeception));
    }
});


/**
 * The following section is for function calls to the Linux Subsystem. 
 */

function addEventTimestamp(timestamp)
{

}

function addEventStart(timestamp,configuration)
{

}

function addEventStop(timestamp)
{

}

function addEventShutdown(timestamp)
{

}

function ConfigurationUpdated(configuration)
{

}

function addEventSystemOn(timestamp, configuration)
{

}

function addEventSystemOff(timestamp, configuration)
{
    
}

function getSlaveList()
{
    
}

function getNodeList()
{
    
}

function getNodeParameters(nodeid)
{
    
}
