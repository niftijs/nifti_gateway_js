/*!
*   \brief A simple plugin structure to use to load endpoints into Express and hence have the endpoint in their own file which makes 
*           setup and control of the system simpler than a single file containing all items. 
*
*/

function expressModule(expressEngine) {
        //private values go in here
        var moduleversion = 1000;
        var endpoint = "/Express/Module";
        var app = expressEngine.app;
        var globals = expressEngine.getGlobals();

         console.log("Express Module :"+endpoint+" version "+moduleversion);

        this.getEndpoint = function(){
            return endpoint;
        }


        this.getVersionID = function(){
            return moduleversion;
        }
        this.getHandler = function(request,response){
                 var taskId = request.params.id;
                try {
                    response.json("{}");
                    console.log(request.body);
                } catch (exeception) {
                    response.send(404);
                }
        }

         this.setHandler = function(request,response){
                 var taskId = request.params.id;
                try {
                    response.json("{}");
                    console.log(request.body);
                } catch (exeception) {
                    response.send(404);
                }
        }
        
        expressEngine.loadmodule(this);
}; //close of function and define 

module.exports = expressModule;