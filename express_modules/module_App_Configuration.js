/*!
*   \brief A simple plugin structure to use to load endpoints into Express and hence have the endpoint in their own file which makes 
*           setup and control of the system simpler than a single file containing all items. 
*
*/

function express_configuration(expressEngine) {
        //private values go in here
        var moduleversion = 1000;
        var endpoint = "/App/Configuration";
        var app = expressEngine.app;

         console.log("Express Module :"+endpoint+" version "+moduleversion);
         var globals = expressEngine.getGlobals();

        this.getEndpoint = function(){
            return endpoint;
        }


        this.getVersionID = function(){
            return moduleversion;
        }

        this.getHandler = function(request,response){
                var taskId = request.params.id;
                try {
                    if(globals == null)
                    {
                          response.json("{\"error\":\"NIFTI Singleton Load Error\"}");
                          return;
                    }
                    var globalconfig = globals.getConfiguration();

                    if(globalconfig != null)
                    {
                            //send the configuration back
                            response.json(globalconfig);
                    }
                    else
                    {
                         response.json("{\"error\":\"not Configuration in System\"}");
                    }
                } catch (exeception) {
                     response.json("{\"error\":\"Command Invalid\"}");
                }
        }

         this.setHandler = function(request,response){
                
                try {
					 var config = request.body;
                    if(globals == null)
                    {
                          response.json("{\"error\":\"NIFTI Singleton Load Error\"}");
                          return;
                    }
					
					var resp = ConfigurationDecoder(config);
                    response.json(resp);
                } catch (exeception) {
                     response.json("{\"error\":\"Command Invalid\"}");
                }
        }
		/*
{
	"task":{
		"airframe":{
			"name":"Hawk 127"
		},
		"taskUuid":"ADF0B373-87F9-4F4C-A14F-5897D5F5D903",
		"name":"Hello",
		"identifier":"ADF0B373-87F9-4F4C-A14F-5897D5F5D903",
		"aircraftSetup":{
            "identifier":"E943FABD-181B-44A4-8694-B420AFE99801",
			"configurations":[
				{
                    "identifier":"C5CAFD8F-BD0F-47B6-9C5F-A5363A8ECC8D",
                    "activeNodes":[
						{
							"nodeIdentifier":"40EBC68C-FD82-45BF-9216-9780CAF8BDE0",
							"channels":[
								{
									"enabled":true,
									"euAlgorithm":"None",
									"name":"x",
									"samplingRate":128,
									"transmisionRate":128
								},
								{
									"enabled":true,
									"euAlgorithm":"None",
									"name":"y",
									"samplingRate":128,
									"transmisionRate":128
								},
								{
									"enabled":true,
									"euAlgorithm":"None",
									"name":"z",
									"samplingRate":128,
									"transmisionRate":128
								}
							]
						},
						{
							"nodeIdentifier":"BBF6BECC-AC97-41A3-9542-5FD6CBED3932",
							"channels":[
								{
									"enabled":true,
									"euAlgorithm":"None",
									"name":"x",
									"samplingRate":128,
									"transmisionRate":128
								},
								{
									"enabled":false,
									"euAlgorithm":"None",
									"name":"y",
									"samplingRate":128,
									"transmisionRate":128
								},
								{
									"enabled":true,
									"euAlgorithm":"None",
									"name":"z",
									"samplingRate":128,
									"transmisionRate":128
								}
							]
						}
					]
				},
				{
                    "identifier":"16BF6574-CF76-4E42-B7B3-CB435C439E85",
                    "activeNodes":[
						{
							"nodeIdentifier":"40EBC68C-FD82-45BF-9216-9780CAF8BDE0",
							"channels":[
								{
									"enabled":true,
									"euAlgorithm":"None",
									"name":"x",
									"samplingRate":128,
									"transmisionRate":128
								},
								{
									"enabled":false,
									"euAlgorithm":"None",
									"name":"y",
									"samplingRate":128,
									"transmisionRate":128
								},
								{
									"enabled":false,
									"euAlgorithm":"None",
									"name":"z",
									"samplingRate":128,
									"transmisionRate":128
								}
							]
						},
						{
							"nodeIdentifier":"BCD809D3-9E2A-48D8-838E-2CE2E17E3D3B",
							"channels":[
								{
									"enabled":false,
									"euAlgorithm":"None",
									"name":"x",
									"samplingRate":128,
									"transmisionRate":128
								},
								{
									"enabled":true,
									"euAlgorithm":"None",
									"name":"y",
									"samplingRate":128,
									"transmisionRate":128
								},
								{
									"enabled":true,
									"euAlgorithm":"None",
									"name":"z",
									"samplingRate":128,
									"transmisionRate":128
								}
							]
						}
					]
				}
			]
		}
	}
}
*/
		function ConfigurationDecoder(configuration)
		{
			try{
					//do we already have a configuration?
					var config = globals.getConfiguration();
					if(config !=null )
					{
						//check the taskuid. 
						if(config.task.taskUuid == configuration.task.taskUuid)
						{
							//they are the same config. return. 
							return "{\"error\":\"Same Configuration\"}";
						}

					}
					//verification of the correct format of the configuration
					if(configuration.task.hasOwnProperty('taskUuid')== false)
      				 {
							return "{\"error\":\"Missing taskUuid\"}";
					 }
					 if(configuration.task.hasOwnProperty('name')== false)
      				 {
							return "{\"error\":\"Missing name\"}";
					 }
					if(configuration.task.hasOwnProperty('identifier')== false)
      				 {
							return "{\"error\":\"Missing identifier\"}";
					 }
					 if(configuration.task.hasOwnProperty('aircraftSetup')== false)
      				 {
							return "{\"error\":\"Missing aircraftSetup\"}";
					 }
					 if(configuration.task.aircraftSetup.hasOwnProperty('identifier')==false)
					 {
						 	return "{\"error\":\"Missing aircraftSetup.identifier\"}";
					 }
					 if(configuration.task.aircraftSetup.hasOwnProperty('configurations')==false)
					 {
						 	return "{\"error\":\"Missing aircraftSetup.configurations\"}";
					 }
					 if(configuration.task.aircraftSetup.hasOwnProperty('configurations')==false)
					 {
						 
					 }

					//setup the active nodes in the system. This is done by walking through the nodes that are in the node list
					//
					var activenodelist = [];
					var configcount = configuration.task.aircraftSetup.configurations.length;
					globals.ClearActiveNodeList();
					if(configcount == 0)
					{
							return "{\"error\":\"Missing aircraftSetup.configurations.items\"}";
					}
					var index = 0;

					for(index = 0; index < configcount; index++)
					{
						var nodecount = configuration.task.aircraftSetup.configurations[index].activeNodes.length;
						var nodeindex = 0;
						for(nodeindex = 0; nodeindex < nodecount; nodeindex++)
						{
								//get the node identifier in the system. 
								var node = configuration.task.aircraftSetup.configurations[index].activeNodes[nodeindex];
								var ActiveNode = new Object();
								ActiveNode.nodeIdentifier = node.nodeIdentifier;
								globals.AddActiveNodeList(ActiveNode);
						}
					}
					globals.setConfiguration(configuration);
					return "{\"error\":\"Success\"}";
			}
			catch(exception)
			{
				return JSON.stringify(exception);




			}
		}

		this.CreateNode = function(nodeid)
		{
			//get the node identifier from the node id. 



		}



        
        expressEngine.loadmodule(this);
}; //close of function and define 

module.exports = express_configuration;

