/*!
*   \brief Charting Endpoint to request a specific body of data to be shown on the graphs. The Charting Endpoint contains the following information
*
*
*   //channel ID is the channel ID of the chart based on the configuration that is currently loaded. 
*
*
*
*   GET: {"channel":channelid,
        "timestart":starttime,
        "duration":milliseconds}
*       
*  RESPONSE: {"channel":channelid,"timestart":starttime,channeldata[]}
*/

function express_charting(expressEngine) {
        //private values go in here
        var moduleversion = 1000;
        var endpoint = "/App/Charting";
        var app = expressEngine.app;

         console.log("Express Module :"+endpoint+" version "+moduleversion);
         var globals = expressEngine.getGlobals();

        this.getEndpoint = function(){
            return endpoint;
        }


        this.getVersionID = function(){
            return moduleversion;
        }

        this.getHandler = function(request,response){
                 var taskId = request.params.id;
                try {
                    if(globals == null)
                    {
                          response.json("{\"error\":\"NIFTI Singleton Load Error\"}");
                          return;
                    }

                    //decode the channelID. 
                    var datastore = app.getDataStore();


                   
                } catch (exeception) {
                     response.json("{\"error\":\"Command Invalid\"}");
                }
        }

         this.setHandler = function(request,response){
                  var taskId = request.params.id;
                try {
                    if(globals == null)
                    {
                          response.json("{\"error\":\"NIFTI Singleton Load Error\"}");
                          return;
                    }
                    var datastore =  globals.getDataFile();
                    var databody = request.body;
                    //search through the datafile until we find the index (channel enabled) that we want
                    var count = 0;
                    var i = datastore.length;
                    for(i = 0; i< datastore.length; i++)
                    {
                            for(j = 0; j < datastore[i].Channels.length; j++)
                            {
                                if(datastore[i].Channels[j].enabled == true)
                                {
                                    if(count == databody.channel)
                                    {
                                            //we have got the correct channel. send back the data starting from the timestamp. 
                                            if(databody.timestart < datastore[i].Channels[j].datastore[0].DateTime)
                                            {
                                                //currently returns only 300 units on the graph. 
                                                var returnarray =datastore[i].Channels[j].datastore.slice(0,300);
                                                response.json(JSON.stringify(returnarray));
                                                return;
                                            }
                                            else
                                            {
                                                //try to find a date time that matches the datetime requested. 
                                                if(databody.timestart < datastore[i].Channels[j].datastore[datastore[i].Channels[j].datastore.length - 1].DateTime)
                                                {
                                                        var startindex = 0;
                                                        for(startindex = 0; startindex < datastore[i].Channels[j].datastore.length; startindex++)
                                                        {
                                                              if(datastore[i].Channels[j].datastore[startindex].DateTime > databody.timestart)    
                                                              {
                                                                    var returnarray =datastore[i].Channels[j].datastore.slice(startindex,300);
                                                                    response.json(JSON.stringify(returnarray));
                                                                    return;
                                                              }  
                                                        }
                                                }
                                            }
                                    }
                                }
                            }
                    }
                   response.json("{\"error\":\"Time Not Found\"}");

                } catch (exeception) {
                     response.json("{\"error\":\"Command Invalid\"}");
                }
        }
        
        expressEngine.loadmodule(this);
}; //close of function and define 

module.exports = express_charting;