/*!
*   \brief A simple plugin structure to use to load endpoints into Express and hence have the endpoint in their own file which makes 
*           setup and control of the system simpler than a single file containing all items. 
*
*/

function express_active(expressEngine) {
        //private values go in here
        var moduleversion = 1000;
        var endpoint = "/App/Command/Active";
        var app = expressEngine.app;

         console.log("Express Module :"+endpoint+" version "+moduleversion);
         var globals = expressEngine.getGlobals();

        this.getEndpoint = function(){
            return endpoint;
        }


        this.getVersionID = function(){
            return moduleversion;
        }

        this.getHandler = function(request,response){
                 var taskId = request.params.id;
                try {
                    if(globals == null)
                    {
                          response.json("{\"error\":\"NIFTI Singleton Load Error\"}");
                          return;
                    }
                    var global_state = globals.getSystemState();

                    var stateResponse = {
                        state:global_state
                    };
                    var resp = JSON.stringify(stateResponse);
                    response.json(resp);
                } catch (exeception) {
                     response.json("{\"error\":\"Command Invalid\"}");
                }
        }

         this.setHandler = function(request,response){
                  var taskId = request.params.id;
                try {
                    if(globals == null)
                    {
                          response.json("{\"error\":\"NIFTI Singleton Load Error\"}");
                          return;
                    }
                    globals.setSystemState(globals.SystemStateEnum.ACTIVE);
                    response.json("{\"state\":\"active\"}");
                } catch (exeception) {
                     response.json("{\"error\":\"Command Invalid\"}");
                }
        }
        
        expressEngine.loadmodule(this);
}; //close of function and define 

module.exports = express_active;