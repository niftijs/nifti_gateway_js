/*!
*       \brief The Express Engine is responsible for creating and handling the serving of data endpoint to the application that will attach to the gateway. The Express Engine
*               communciates to the etherCatEngine and uses it for sending and receiving commands over the backplane to the modules and nodes that are in the system. 
*
*
*/

function expressEngine(SystemGlobals) {
        //private values go in here
        var moduleversion = 1000;
        var LISTENPORT = 1234;
        var STREAMWEBSOCKET = 8080;
        var NODEWEBSOCKET = 8081

        //Boiler Plate Setup Code for the Express Engine
        var express = require('express');
        var app = express();
        var http = require('http').Server(app);
        var WebSocketServer = require('ws').Server;
        var wss = new WebSocketServer({
            port: STREAMWEBSOCKET
        });

        var node_wss = new WebSocketServer({
            port: NODEWEBSOCKET
        });

        var bodyParser = require('body-parser');

        app.use(express.static('public'));

        app.get('/', function(req, res) {
            res.sendFile(__dirname + '/index.html');
        });
 
        http.listen(LISTENPORT, function() {
            console.log('listening on *:'+LISTENPORT);
        });

        app.use(bodyParser.urlencoded({
            extended: true
        }));
        app.use(bodyParser.json());

        console.log("Express Engine "+moduleversion);

        var SystemGlobalVars = SystemGlobals;

        //load modules into express. 

        this.loadmodule = function(expressmodule){
            var endpoint = expressmodule.getEndpoint();
            var gethandler = expressmodule.getHandler;
            app.get(endpoint,gethandler);
            var sethandler = expressmodule.setHandler;
            app.post(endpoint,sethandler);

        }
 
        this.Initialise = function(){

        }

        this.getVersionID = function(){
            return moduleversion;
        }

        this.getGlobals = function(){
            return SystemGlobalVars;
        }

        this.getDataStore = function(){
              var ECE = SystemGlobalVars.getEtherCatEngine();
                var datastore = ECE.getNodeData();
                return datastore;
        }

              //this function sends out raw data in the correct format when the system is in run mode. 
        /*!
        *   \brief ConvertInt32toByteArray
        *   \param x The Int32 variable we are converting to a byte array
        */
        function ConvertInt32toByteArray(x)
        {
            var bytes = new Uint8Array(4);
            var i = 0;
            do {
                bytes[i++] = x & (255);
                x = x>>8;
            } while ( i < 4)
            return bytes;
        }

        /*!
        *   \brief ConvertInt16toByteArray
        *   \param x The Int16 variable we are converting to a byte array
        */
        function ConvertInt16toByteArray(x)
        {
            var bytes = new Uint8Array(2);
            var i = 0;
            do {
                bytes[i++] = x & (255);
                x = x>>8;
            } while ( i < 2 )
            return bytes;
        }

        /*!
        *   \brief AddToArray
        *   \param[in] inarray The inarray
        *   \param[in] addarray The array of elements we are adding to the inarray
        *   \return The array of data we are returning. 
        */
        function AddToArray(inarray, addarray)
        {
            var length = 0;
            if(inarray == null)
            {
                length = addarray.length
            }
            else
            {
                length = inarray.length + addarray.length;
            }
            var arrout = new Uint8Array(length);
            //copy the inarray into arrout. 
            var i = 0;
            for(i = 0; i < inarray.length; i++)
            {
                arrout[i] = inarray[i];
            }
            for(i = 0; i < addarray.length; i++)
            {
                arrout[i + inarray.length] = addarray[i];
            }
            return arrout;
        }

        wss.broadcast = function broadcast(data) {
            wss.clients.forEach(function each(client) {
                client.send(data);
            });
        };

        node_wss.broadcast = function broadcast(data){
            node_wss.clienats.forEach(function each(client){
                client.send(data);
            })
        }

        // Send to Everyone. 
        //This function is responsible for sending data out to the application from a configuration. 
        setInterval(function(){
            var systemstate = SystemGlobalVars.getSystemState();
            if(systemstate >= 3)
            {
                    var d = new Date();
                    // Computing timestamp by reducing UNIX timestamp bytes. This number must be added later.
                    var milliseconds = Math.round(d.getTime()) - 1502200000*1000; 
                    SystemTimeStamp = milliseconds;

                    var bytearray  = [];
                    testingindexer = Math.random();
                    bytearray = AddToArray(bytearray,ConvertInt32toByteArray(SystemTimeStamp));
                    //create a datastream for decoding. 
                    var i = 0;

                    var ECE = SystemGlobalVars.getEtherCatEngine();
                    var datastore = ECE.getNodeData()
                    for(i = 0; i <  datastore.length; i++)
                    {
                        //this is a MobileJazz style node. The assumption is if the node is in the list then a channel on the node is enabled.
                        //add node data to the data stream as per the document on ASANA
                        var rssi = datastore[i].Rssi;
                        var battery = datastore[i].battery;
                        var batteryremainingseconds = datastore[i].RemainingBatteryTime;
                        var temperature = datastore[i].Temperature;

                        bytearray = AddToArray(bytearray,ConvertInt16toByteArray(rssi));
                        bytearray = AddToArray(bytearray,ConvertInt16toByteArray(battery));
                        bytearray = AddToArray(bytearray,ConvertInt32toByteArray(batteryremainingseconds));
                        bytearray = AddToArray(bytearray,ConvertInt16toByteArray(temperature));

                        // Debug values: showing a sinusodial signal for all channels

                        var j = 0;
                        for(j = 0; j < datastore[i].Channels.length; j++)
                        {
                            if(datastore[i].Channels[j].enabled == 1)
                            {
                                //the value of the item / 1000. so 1000 == 1
                                bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*datastore[i].Channels[j].value));
                                bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*datastore[i].Channels[j].maxval));
                                bytearray = AddToArray(bytearray,ConvertInt16toByteArray(1000*datastore[i].Channels[j].minval));
                                bytearray = AddToArray(bytearray,ConvertInt16toByteArray(datastore[i].Channels[j].alarm));
                            }
                        }
                    }
                    wss.broadcast(bytearray);
            }
        }, 32 );


}; //close of function and define 

module.exports = expressEngine;