/*!
*   \brief A simple plugin structure to use to load endpoints into Express and hence have the endpoint in their own file which makes 
*           setup and control of the system simpler than a single file containing all items. 
*
*/

/*
*   \system on sets the configuration that is going to be used to turn the system on.   System On has the system active, streaming data but not recording the data. 
*           resend systemon to move from RECORDING to SYSTEMON. 
*
*/

function express_systemon(expressEngine) {
        //private values go in here
        var moduleversion = 1000;
        var endpoint = "/App/Command/Start";
        var app = expressEngine.app;

         console.log("Express Module :"+endpoint+" version "+moduleversion);
         var globals = expressEngine.getGlobals();

        this.getEndpoint = function(){
            return endpoint;
        }


        this.getVersionID = function(){
            return moduleversion;
        }

        this.getHandler = function(request,response){
                 var taskId = request.params.id;
                try {
                    if(globals == null)
                    {
                          response.json("{\"error\":\"NIFTI Singleton Load Error\"}");
                          return;
                    }
                    var global_state = globals.getSystemState();

                    var stateResponse = {
                        state:global_state
                    };
                    var resp = JSON.stringify(stateResponse);
                    response.json(resp);
                } catch (exeception) {
                     response.json("{\"error\":\"Command Invalid\"}");
                }
        }

         this.setHandler = function(request,response){
                  var taskId = request.params.id;
                try {
                    if(globals == null)
                    {
                          response.json("{\"error\":\"NIFTI Singleton Load Error\"}");
                          return;
                    }

                     var databody = request.body;

                     //get the configuration. If there is no configuration then return an error
                     	var config = globals.getConfiguration();
                        if(config ==null )
                        {
                            response.json("{\"error\":\"Need a configuration loaded first\"}");
                            return;
                        }
                        //get the configuration to be run from the configuration section of the request. 
                        var configurationid = databody.identifier;

                        //set the system configuration to the configurationid. 
                        var res = globals.setActiveConfiguration(configurationid);
                        globals.setSystemState(3);
                        response.json(res);
                        return;
                } catch (exeception) {
                     response.json("{\"error\":\"Command Invalid\"}");
                }
        }
        
        expressEngine.loadmodule(this);
}; //close of function and define 

module.exports = express_systemon;