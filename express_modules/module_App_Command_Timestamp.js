/*!
*   \brief A simple plugin structure to use to load endpoints into Express and hence have the endpoint in their own file which makes 
*           setup and control of the system simpler than a single file containing all items. 
*
*/

function express_timestamp(expressEngine) {
        //private values go in here
        var moduleversion = 1000;
        var endpoint = "/App/Command/TimeStamp";
        var app = expressEngine.app;

         console.log("Express Module :"+endpoint+" version "+moduleversion);
         var globals = expressEngine.getGlobals();

        this.getEndpoint = function(){
            return endpoint;
        }


        this.getVersionID = function(){
            return moduleversion;
        }

        this.getHandler = function(request,response){
                 var taskId = request.params.id;
                try {
                    if(globals == null)
                    {
                          response.json("{\"error\":\"NIFTI Singleton Load Error\"}");
                          return;
                    }
                    var SystemTimeStamp = globals.getSystemTime();
                    var timestampresponse = {
                        timestamp:SystemTimeStamp
                    };
                    var timestamp = JSON.stringify(timestampresponse);
                    response.json(timestamp);
                } catch (exeception) {
                     response.json("{\"error\":\"System Time Stamp Incorrect\"}");
                }
        }

         this.setHandler = function(request,response){
                 var taskId = request.params.id;
                try {
                     var timestamp = request.body;
                    if(timestamp == null)
                    {
                        response.json("{\"error\":\"Null Body\"}");
                        return;
                    }
                    else if(timestamp.timestamp == null)
                    {
                        response.json("{\"error\":\"Invalid Time (Does it Exist?)\"}");
                        return;

                    }
                    NIFTI.setSystemTime(timestamp.timestamp);
                } catch (exeception) {
                    response.json("{\"error\":\"Critical Error - Catch Caught Problem\"}");
                }
        }
        
        expressEngine.loadmodule(this);
}; //close of function and define 

module.exports = express_timestamp;