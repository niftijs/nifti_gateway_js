/*!
*   \brief Endpoint for the application to query the gateway and get the gateway settings. The gateway settings are as follows
*
*   This function will return the entire gateway object as one large object, with all node and gateway card data combined in one object. 
*/

function express_gateway(expressEngine) {
        //private values go in here
        var moduleversion = 1000;
        var endpoint = "/App/Gateway";
        var app = expressEngine.app;

         console.log("Express Module :"+endpoint+" version "+moduleversion);
         var globals = expressEngine.getGlobals();

        this.getEndpoint = function(){
            return endpoint;
        }


        this.getVersionID = function(){
            return moduleversion;
        }

        this.getHandler = function(request,response){
                 var taskId = request.params.id;
                try {
                    if(globals == null)
                    {
                          response.json("{\"error\":\"NIFTI Singleton Load Error\"}");
                          return;
                    }

                    //return the gateway object. 
                    var gatewayobject = globals.getMasterData();
                    var gatewayobj = JSON.stringify(gatewayobject);
                    response.json(gatewayobj);
                } catch (exeception) {
                    console.log(exception);
                     response.json("{\"error\":\"Command Invalid\"}");
                }
        }

         this.setHandler = function(request,response){
                  var taskId = request.params.id;
                try {
                    if(globals == null)
                    {
                          response.json("{\"error\":\"NIFTI Singleton Load Error\"}");
                          return;
                    }
                    response.json("{\"error\":\"You can only read gateway data. try module indexing\"}");
                } catch (exeception) {
                     response.json("{\"error\":\"Command Invalid\"}");
                }
        }
        
        expressEngine.loadmodule(this);
}; //close of function and define 

module.exports = express_gateway;