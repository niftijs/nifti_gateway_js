/*!
*   \brief An Event is when the FTE presses the event button on the iPAD application to timestamp an event in the system. The JSON format is as follows
*
*   {"timestamp":11232342322,"data":"button press event"}
*
*
*
*
*/

function express_event(expressEngine) {
        //private values go in here
        var moduleversion = 1000;
        var endpoint = "/App/Command/Event";
        var app = expressEngine.app;

         console.log("Express Module :"+endpoint+" version "+moduleversion);
         var globals = expressEngine.getGlobals();

        this.getEndpoint = function(){
            return endpoint;
        }


        this.getVersionID = function(){
            return moduleversion;
        }

        this.getHandler = function(request,response){
                 var taskId = request.params.id;
                try {
                     response.json("{\"error\":\"Command Invalid\"}");
                } catch (exeception) {
                     response.json("{\"error\":\"Command Invalid\"}");
                }
        }

         this.setHandler = function(request,response){
                  var taskId = request.params.id;
                try {
                    if(globals == null)
                    {
                          response.json("{\"error\":\"NIFTI Singleton Load Error\"}");
                          return;
                    }
                    /*
                    if(globals.getSystemState() != globals.SystemStateEnum.RECORDING)
                    {
                         response.json("{\"error\":\"System is not Recording\"}");
                          return;
                    }
                    */
                     var eventdata = request.body;

                     console.log(eventdata);

                     if(eventdata.timestamp)
                     {
                            if(globals.storeEvent(eventdata)==true)
                            {
                                response.json("{\"event\":\"stored\"}");
                            }
                            else
                            {
                                response.json("{\"error\":\"not ready\"}");
                            }
                     }

                } catch (exeception) {
                     response.json("{\"error\":\"Command Invalid\"}");
                }
        }
        
        expressEngine.loadmodule(this);
}; //close of function and define 

module.exports = express_event;