
//load the global NIFTI instance. 
var globalNIFTI = require('./nifti_modules/globalNifti');
var GLOBALNIFTI = new globalNIFTI();

var etherCatEngine = require('./linux_modules/etherCatEngine');

//load the EtherCat Module in the system. 
var ECE = new etherCatEngine();
ECE.Initialise();
if(ECE.getVersionID() != 1234)
{
    console.log("EtherCat Engine Version MisMatch");
}
else
{
    console.log("EtherCat Engine Version " + ECE.getVersionID());
}

var ExpressEngine = require('./express_modules/expressEngine');
var EE = new ExpressEngine(GLOBALNIFTI);

//now add some endpoints to the Express Engine. 

var expressModule = require("./express_modules/expressModule");
var rootModule = new expressModule(EE);

var appCommandDateTimeModule = require("./express_modules/module_App_Command_Timestamp");
var cmdDateTimeModule = new appCommandDateTimeModule(EE);

var appCommandShutdown = require("./express_modules/module_App_Command_Shutdown");
var cmdShutdown = new appCommandShutdown(EE);

var appCommandActive = require("./express_modules/module_App_Command_Active");
var cmdActive = new appCommandActive(EE);

var appCommandReady = require("./express_modules/module_App_Command_Ready");
var cmdReady = new appCommandReady(EE);

var appCommandRecording = require("./express_modules/module_App_Command_Recording");
var cmdReady = new appCommandRecording(EE);

var appCommandSystemOn = require("./express_modules/module_App_Command_SystemOn");
var cmdSystemOn = new appCommandSystemOn(EE);

var appCommandSystemOff = require("./express_modules/module_App_Command_SystemOff");
var cmdSystemOff = new appCommandSystemOff(EE);

var appCommandEvent = require("./express_modules/module_App_Command_Event");
var cmdEvent = new appCommandEvent(EE);

// var appCommandnode = require("./express_modules/module_App_Node");
// var cmdNode = new appCommandnode(EE);

var appCommandConiguration = require("./express_modules/module_App_Configuration");
var cmdConfiguration = new appCommandConiguration(EE);

var appCommandGateway = require("./express_modules/module_App_Gateway");
var cmdGateway = new appCommandGateway(EE);

var appCharting = require("./express_modules/module_App_Charting");
var cmdChart = new appCharting(EE);

var nifti_module = require("./nifti_modules/nifti_gateway");
var niftimodule = new nifti_module();

var simulator = require("./linux_modules/etherCatSimulator");
var sim = new simulator(niftimodule,GLOBALNIFTI);

//initialise the simulator. 
sim.Initialise();

//change this between the simulation mode or the talking to the linux subsystem modes. 
GLOBALNIFTI.setEtherCatEngine(sim);

//update the simulator every 10ms. 
setInterval(function()
{
    sim.Runtime();
},10);
