/*!
*   GLOBAL
*
*   The global function is a global static singleton store of status and other items of interest in the system. 
*
*/
function globalNIFTI(){
        //private values go in here
        var moduleversion = 1234;

        //system state enumeration
        var SystemStateEnum  = {
            IDLE: 1,
            READY : 2,
            SYSTEMON: 3,
            ACTIVE:4,
            RECORDING:5,
            SHUTDOWN: 6
        };

        var SystemConfiguration = null;

        var SystemState = SystemStateEnum.IDLE;

        var EtherCatEngine = null;

        var WebSocketServer = null;

        var activeNodeList = [];

        var ActiveConfiguration = null;

        var ActiveConfigurationChanged = 0;

        this.getActiveConfigurationChanged = function()
        {
            if(ActiveConfigurationChanged == 1)
            {
                ActiveConfigurationChanged = 0;
                return 1;
            }
            return 0;

        }

        this.getActiveConfiguration = function()
        {
            return ActiveConfiguration;
        }


        this.setActiveConfiguration = function(activeconfigid)
        {
            //check the configuration list for the activeconfigid
            if(SystemConfiguration == null)
            {
                return "{\"error\":\"System Configuration Does Not Exist\"}";

            }
            //check the configurations in the SystemConfiguration
            var idx = 0;
            var count = SystemConfiguration.task.aircraftSetup.configurations.length;
            for(idx = 0; idx < count; idx++)
            {
                if(SystemConfiguration.task.aircraftSetup.configurations[idx].identifier == activeconfigid)
                {
                     //get the active nodes and channels. 
                     ActiveConfiguration = SystemConfiguration.task.aircraftSetup.configurations[idx];

                     //decode the system configuration, and record the channels and ids that are active in the system. 
                     ActiveConfigurationChanged = 1;


                     return "{\"configuration\":\"OK\"}";
                }
            }
            
            return "{\"error\":\"Configuration Not Found!\"}";
        }

        this.setWebSocketServer = function(server)
        {
            WebSocketServer = server;
        }
        
        this.getSystemState = function(){
            return this.SystemState;
        }

       

        this.setSystemState = function(systemState){
            this.SystemState = systemState;
        }

        this.Initialise = function(){

        }

        this.getVersionID = function(){
            return moduleversion;
        }

        var SystemEpoch = (new Date).getTime();

        this.getSystemTime = function(){
            SystemEpoch = (new Date).getTime();
            return SystemEpoch;
        }

        this.setSystemTime = function(epoch){
            this.SystemEpoch = epoch;
        }

        function textfunction(){
            return moduleversion;
        }

        this.storeEvent = function(eventmsg){
                if(SystemState == SystemStateEnum.RECORDING)
                {
                    return true;
                }
                else
                {
                    return false;
                }
        }

        this.setConfiguration = function(configuration)
        {
            SystemConfiguration = configuration;
        }

        this.getConfiguration = function(){
            return SystemConfiguration;
        }

        this.getDataFile = function(datafile)
        {
            return "{}";
        }

        this.getMasterData = function()
        {
            return this.EtherCatEngine.getGatewayObject();
        }

        this.setEtherCatEngine = function(EtherCatEngine)
        {
            this.EtherCatEngine = EtherCatEngine;
        }

        this.getEtherCatEngine = function()
        {
            return this.EtherCatEngine;
        }

        this.ClearActiveNodeList = function()
        {
            activeNodeList = [];
        }

        this.AddActiveNodeList = function(ActiveNode)
        {
            //search through the active node list to see if the active node exists. 
            var listindex = 0;
            for(listindex = 0; listindex < activeNodeList.length; listindex++)
            {
                if(activeNodeList[listindex].nodeIdentifier == ActiveNode.nodeIdentifier)
                {
                    return;
                }
            }
            //does not exist. Push it onto the list index. 
            activeNodeList.push(ActiveNode);
            console.log("Node Added : "+ JSON.stringify(ActiveNode));
        }
}; //close of function and define 

module.exports = globalNIFTI;


