


function NiftiGateway()
{
     var moduleversion = 1000;

/*!
*   \brief nifti_gateway is the gateway object that is used in the system to store gateway items. 
*
*   Master:{
    "SerialNumber":"4SkZs4kdjhfJlfjshsaldhshSkfjs",
    "ProductID":"NIFTI Master",
    "HWVersion":"1.0.0",
    "SWVersion","1.0.0",
    "EtherCatEn":1,
    "IPEn":0,
    "Slaves":[NIFTISlave]
}
*/

this.Nifti_CreateGatewayObject =  function(serialnumber,productid,hardwareversion,swversion,ethercat,ip)
{
    var gateway = new Object();
    gateway.SerialNumber = serialnumber;
    gateway.ProductID = productid;
    gateway.HWVersion = hardwareversion;
    gateway.SWVersion = swversion;
    gateway.EtherCatEn = ethercat;
    gateway.IPEn = ip
    gateway.Slaves = [];

    return gateway;
}

this.Nifti_GatewayAddSlave =  function(gateway,slave)
{
    gateway.Slaves.push(slave);
}

/*!
*   \brief Creates a slave object of slave type. It will call the constructor of the slavetype from another file. 
*
*       NIFTISlave:{
            "SerialNumber":"4SkZs4kdjhfJlfjshsaldhshSkfjs",
            "ProductID":"%PRODUCTID%",
            "HWVersion":"1.0.0",
            "SWVersion","1.0.0",
            "Capabilities":[{"%Capabilities%"}]
};
*
*
*
*
*/
this.Nifti_CreateSlave =  function(slavetype,serialnumber,hwversion,swversion)
{
    var slave = new Object();

    slave.SerialNumber = serialnumber;
    slave.SlaveType = slavetype;
    slave.HWVersion = hwversion;
    slave.SWVersion = swversion;
    slave.Capabilities = [];

    if(slavetype == "NGW-02")
    {
        
    }

    return slave;

}
/*
Capabilities:{
                "name":"%Capability",
                ...
            }
*/
this.Nifti_CreateCapability = function(name)
{
    var cap = new Object();
    cap.name = name;
    return cap;

}
/*
PowerSource:{
        "VoltageIn":"3.45",
        "CurrentIn":"890",
        "Capacity":"-1",
        "VoltageOut3v3":4.98,
        "CurrentOut3v3":1234
        "VoltageOut5v0":4.98,
        "CurrentOut5v0":1234,
        "temperature":22.1
}
*/
this.Nifti_CreateCap_PowerSource =   function(vin,cin,capacity,vouti,couti,voutii,coutii,temperature)
{
    var cap = this.Nifti_CreateCapability("PowerSource");
    cap.VoltageIn = vin;
    cap.CurrentIn = cin;
    cap.Capacity = capacity;
    cap.VoltageOut3V3 = vouti;
    cap.CurrentOut3V3 = couti;
    cap.VoltageOut5V0 = voutii;
    cap.CurrentOut5V0 = coutii;
    cap.temperature = temperature;

    return cap;
}
/*
PowerIn:{
        "VoltageIn3v3":4.98,
        "CurrentIn3v3":1234
        "VoltageIn5v0":4.98,
        "CurrentIn5v0":1234,
        "temperature":22.1
}
*/
this.Nifti_CreateCap_PowerIn = function(vini,cini,vinii,cinii,temperature)
{
    var cap = this.Nifti_CreateCapability("PowerIn");
    cap.VoltageIn3V3 = vini;
    cap.CurrentIn3V3 = cini;
    cap.VoltageIn5V0 = vinii;
    cap.CurrentIn5V0 = cinii;
    cap.temperature = temperature;

    return cap;
}

/*!
*
*   DataStore:{
    "Mounted":yes,
    "Capacity":14000000,
    "FreeSpace":12500000
*
*
*/

this.Nifti_CreateCap_DataStore = function(mounted,capacity,freespace)
{
     var cap = this.Nifti_CreateCapability("DataStore");
     cap.Mounted = mounted;
     cap.Capacity = capacity;
     cap.FreeSpace = freespace;

     return cap;
}
/*!
*
* 
PowerStore:{
    "RequiredPower":450,
    "MaxCapacity":4500,
    "CurrentCapacity":3200
}
*
*
*/
this.Nifti_CreateCap_PowerStore =  function(requiredpower,maxcapacity,currentcapacity)
{
     var cap = this.Nifti_CreateCapability("PowerStore");
     cap.RequiredPower = requiredpower;
     cap.MaxCapacity = maxcapacity;
     cap.CurrentCapacity = currentcapacity;

     return cap;
}

/*
NiftiLink:{
    "NodeList":[NIFTINODE]
}
*/
this.Nifti_CreateCap_NiftiLink =  function(ScanChannels)
{
     var cap = this.Nifti_CreateCapability("NiftiLink");
     cap.scanChannels = [];
     cap.scanChannels = ScanChannels;
     cap.NodeList = [];

     return cap;
}
/*
NiftiNode:{
    "SerialNumber":"4SkZs4kdjhfJlfjshsaldhshSkfjs",
    "Battery":0 - 100 %,
    "Storage":0 - 100 %,
    "Rssi":0-100%,
    "State":Sleep,Active,Running,
    "transRate"::2^n,
    "Channels":[NIFTICHANNEL]
}

NiftiChannel:{
    "enabled":true,
    "samplingRate":2^n,
    "name":"x-axis",
    "algo":"none""
}
*/
this.Nifti_CreateNode = function(serialnumber,battery,storage,rssi,state,transmissionrate)
{
    var node = new Object();
    node.SerialNumber = serialnumber;
    node.Battery = battery;
    node.Storage = storage;
    node.Rssi = rssi;
    node.State = state;
    node.transRate = transmissionrate;
    node.Channels = [];

    return node;
}

this.Nifti_CreateChannel =  function(enabled,sampleRate,name,algo,maxrate)
{
    var channel = new Object();
    channel.enabled = enabled;
    channel.sampleRate = sampleRate;
    channel.name = name;
    channel.algo = algo;

    return channel;
}

this.Nifti_CreateAccelerometer =  function(serialnumber,battery,storage,rssi,state,transmissionrate)
{
    var accelerometer = this.Nifti_CreateNode(serialnumber,battery,storage,rssi,state,transmissionrate);
    //add the 5 channels
    var channelx = this.Nifti_CreateChannel(false,0,"X","none",1024);
    var channely = this.Nifti_CreateChannel(false,0,"Y","none",1024);
    var channelz = this.Nifti_CreateChannel(false,0,"Z","none",1024);
    var TCore = this.Nifti_CreateChannel(false,0,"TCore","none",1);
    var TSkin = this.Nifti_CreateChannel(false,0,"TSkin","none",1);
    accelerometer.Channels.push(channelx);
    accelerometer.Channels.push(channely);
    accelerometer.Channels.push(channelz);
    accelerometer.Channels.push(TCore);
    accelerometer.Channels.push(TSkin);

    return accelerometer;
}
};

module.exports = NiftiGateway;