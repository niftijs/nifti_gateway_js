FROM node:9.5.0-alpine

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY package.json .

RUN npm install

COPY . .

EXPOSE 8080 1234

CMD [ "npm", "start" ]