var fs = require('fs');
const pg = require('pg');
var readline = require("readline");
var stream = require('stream');
var events = require('events');
var eventEmitter = new events.EventEmitter();
var NiftiMainIp = "127.0.0.1", NiftiMainSendPort = 7700, NiftiMainRecvPort = 7701;
var dgram = require( "dgram" ); 
var NiftiMainUdpConn = dgram.createSocket( "udp4" );
const { spawn } = require('child_process');
var I2C_SLAVE_M0  = 0x6e;
var I2C_SLAVE_LED = 0x18;

// Stitch
// two arrays to store data
var config = null;
var pool = null;
var cfcard      = []; // timestamp
var cfcard_data = [[]]; // actual data
var cfcard_idx  = 0;
var loading_idx = 0;
var UID_MAX_DATA = 24;
var MIN_TSTAMP_DIFF = 5;
var tracknum = 0;

var EVENT_FILE_PATH1  = "/run/media/cfcard1/nifti_events.txt";
var EVENT_FILE_PATH2  = "/run/media/cfcard2/nifti_events.txt";
var CFCARD1_FILE_PATH = "/run/media/cfcard1/wave.csv";
var CFCARD2_FILE_PATH = "/run/media/cfcard2/wave.csv";
var SDCARD_FILE_PATH = "/run/media/mmcblk0p2/wave.csv";

const SET_NODE_CONFIG  = 0x42;
const SET_CONFIG       = 0x43;

var check_rf_ws_count   = 0;
var prev_rf_ws_count    = 0;
const SEND_CONFIG_DELAY = 3;

var l_slave_ngw05 = null;
var slave_ngw05_SerialNumber = "1";

var niftidb_config = {
    user: 'niftidb', 
    database: 'nifti_streamout_log', 
    password: 'nifti', 
    host: 'localhost', 
    max: 10, 
    idleTimeoutMillis: 30000, 
};

var sinewave_idx = 0;

var RF_START_RECORD = 0;
var RF_STOP_RECORD = 0;
var RF_NODE_SHUTDOWN = 0;
var RF_SHUTDOWN = 0;
var RF_IDLE = 0;
var SEND_CONFIGURATION = 0;

var config = null;

niftiudp_connect = function()
{
    console.log ( "niftiudp_connect ");
    NiftiMainUdpConn = dgram.createSocket( "udp4" );
    NiftiMainUdpConn.bind( NiftiMainRecvPort,NiftiMainIp,function (e) 
    {
        if ( e )
        {
            console.error ( e );
        }
        console.log('NIFTI UDP connection is established');
    });
    return NiftiMainUdpConn;
}

pg_connect = function()
{
    pool = new pg.Pool(niftidb_config);
    pool.connect(function(err, client, done) 
    {
        console.log ( "Postgres db connected")
    });
}

 //generated message requests as per ftelink 
generateUDPJsonRequests = function ( cmdid, param)
{
    //( "{ \"tracking_number\": 1, \"command\": 65, \"params\" : { \"mode\": 7 } }" )
    var msg          = {};
    key              = "tracking_number";
    msg[key]         = tracknum;
    key              = "slaveid";
    msg[key]         = 0;
    key              = "command";
    msg[key]         = cmdid;
    key              = "params";
    msg[key]         = {};
    msg[key]         = param; 
    
    tracknum = tracknum + 1;
    return JSON.stringify ( msg );
}

 // to log all NIFTI events on to a file on CF card.
logNiftiEvents = function ( val )
{
    var dt = new Date();
    fs.writeFile(EVENT_FILE_PATH1, dt.toString()+ val,{'flag':'a'}, function(err) 
    {
        if(err) 
        {
            //return console.log(err);
        }
    });
    fs.writeFile(EVENT_FILE_PATH2, dt.toString()+ val,{'flag':'a'}, function(err)
    {
        if(err)
        {
            //return console.log(err);
        }
    }); 
}

// checks the time stamp for stitching
find_closest_tstamp = function ( tstamp )
{
    var idx   = 0;
    var max_idx = cfcard_idx - 1;
    var ret_idx = cfcard_idx + 1;
    if ( ( tstamp - cfcard[max_idx] )  > ( Math.abs ( tstamp - cfcard[0] ) ) )
    {
        idx = max_idx; 
        while ( idx >= 0 )
        {
            if ( Math.abs ( cfcard[idx] - tstamp ) <= MIN_TSTAMP_DIFF ) 
            {
                ret_idx = idx;
                break;
            }
            idx--;            
        }
    }
    else
    {
        idx = 0;
        while ( idx <= cfcard_idx )
        {
            if ( Math.abs ( tstamp -  cfcard[idx] ) <= MIN_TSTAMP_DIFF ) 
            {
                ret_idx = idx;
                break;
            }   
            idx++;            
        }
    }
    return ret_idx;
}

function stitch_data()
{
    // Retrieve value from DB
    var dt1 = new Date();
    cfcard      = []; // timestamp
    cfcard_data = [[]]; // actual data
    cfcard_idx  = 0;
    loading_idx = 0;
    var nodeid   = 0;
    var TIMESTAMP = 0; 
    var uid = 0;
    var max_nodes = config.activeNodes.length;
    var nodes_max_data  = max_nodes * UID_MAX_DATA ;
    var cur_timestamp = 0;
    console.log ( " stitch function STARTED" , dt1.toString());
    pool.query('select * from streamout',function(err, result) {
        if(err) 
        {
            console.error('error running query', err);
        }
        console.log ( " stitch got data from db",result.rowCount );
        for (var i = 0;i < result.rowCount;i++)
        {
            for (var jkey2 in result.rows[i] ) 
            {
                var json_obj1 = result.rows[i][jkey2];
                for ( var jkey1 in json_obj1)
                {
                    TIMESTAMP = 0;
                    cur_timestamp = 0;
                    if ( jkey1 == "result")
                    {
                        var json_obj = json_obj1[jkey1];
                        for (var jkey in json_obj )
                        {
                            if ( jkey == "timestamp")
                            {
                                cur_timestamp = parseInt(json_obj[jkey],16);
                            }
                            else if (jkey == "uid")
                            {
                                uid = json_obj[jkey];
                                for(var j = 0; j <  config.activeNodes.length; j++)
                                {
                                    if ( uid.toString() == config.activeNodes[j].nodeIdentifier )
                                    {
                                        nodeid = j;
                                    }
                                }
                                if( config.activeNodes[nodeid].channels[0].enabled || config.activeNodes[nodeid].channels[1].enabled ||
                                    config.activeNodes[nodeid].channels[2].enabled )
                                {
                                    if ( cfcard_idx == 0 || ( cur_timestamp  >  ( cfcard[cfcard_idx - 1] + MIN_TSTAMP_DIFF ) )  )
                                    {
                                        cfcard[cfcard_idx] = cur_timestamp ;
                                        cfcard_data[cfcard_idx] = [];
                                        for ( var l = 0; l < nodes_max_data; l++ )
                                        {
                                            cfcard_data[cfcard_idx][l] = ',';    
                                        }
                                        loading_idx      = cfcard_idx;
                                        cfcard_idx       = cfcard_idx + 1; 
                                        TIMESTAMP = 1; 
                                    }
                                    else
                                    {
                                        // Find the matching timestamp
                                        var l_idx = find_closest_tstamp( cur_timestamp  );
                                        if ( l_idx <= cfcard_idx )
                                        {
                                            loading_idx = l_idx; 
                                            TIMESTAMP = 1;                                            
                                        }
                                        else
                                        {
                                            TIMESTAMP = 0;    
                                        }
                                    }
                                }
                            }
                            else if ( jkey == "data")
                            {
                                var k = 0;
                                var nodeIdx = ( nodeid ) * UID_MAX_DATA ;
                                if ( TIMESTAMP == 1)
                                {
                                    while ( k < json_obj[jkey].length  )
                                    {
                                        if( config.activeNodes[nodeid].channels[0].enabled )
                                        {
                                            cfcard_data[loading_idx][nodeIdx] = json_obj[jkey][k];     
                                        }
                                        nodeIdx = nodeIdx + 1;
                                        k = k + 1;
                                        if( config.activeNodes[nodeid].channels[1].enabled )
                                        {
                                            cfcard_data[loading_idx][nodeIdx] = json_obj[jkey][k];     
                                        }
                                        nodeIdx = nodeIdx + 1;
                                        k = k + 1;
                                        if( config.activeNodes[nodeid].channels[2].enabled )
                                        {
                                            cfcard_data[loading_idx][nodeIdx] = json_obj[jkey][k];     
                                        }
                                        nodeIdx = nodeIdx + 1;
                                        k = k + 1;
                                    }    
                                }    
                            }
                        }    
                    }     
                }
            }        
        }
        console.log ("timestamp idx = ",cfcard_idx);
        console.log ("data idx = ",cfcard_data.length)
        // Write all data to CF card / SD card.
        var d = 0;
        //var file1 = fs.createWriteStream(CFCARD1_FILE_PATH);
        //var file2 = fs.createWriteStream(CFCARD2_FILE_PATH);
        var file3 = fs.createWriteStream(SDCARD_FILE_PATH);
        //file1.on('error', function(err) { });
        //file2.on('error', function(err) { });
        file3.on('error', function(err) { });
        while ( d < cfcard_idx)
        {
            var tstamp_str = cfcard[d].toString() + ",";
            //file1.write(tstamp_str);
            //file2.write(tstamp_str);
            file3.write(tstamp_str);
            cfcard_data[d].forEach(function(item) { 
                if ( typeof(item) == "number" )
                {
                    item = item.toString();
                }
                //file1.write(item + "," ); 
                //file2.write(item + "," ); 
                file3.write(item + "," ); 
            }); 
            //file1.write("\n");
            //file2.write("\n");
            file3.write("\n");
            d = d + 1;
        }
        //file1.end();
        //file2.end();
        file3.end();
        console.log ( "CF card writing complete");
        console.log ( " stitch function COMPLETED" , dt1.toString());
    });
    return 0;
}

check_niftimain_requests = function ()
{
    if (  SEND_CONFIGURATION == 0 )
    {
        if (RF_NODE_SHUTDOWN   == 1 || RF_SHUTDOWN  == 1 )
        {
            RF_START_RECORD     = 0;
            RF_STOP_RECORD      = 0;
            RF_IDLE             = 0;
        }
        if ( RF_START_RECORD == 1 )
        {
            // RF START
            console.log ( " SEND RF START RECORD COMMAND");
            var message = new Buffer( "{ \"tracking_number\": 1, \"slaveid\" : 0, \"command\": 65, \"params\" : { \"mode\": 4 } }" )
            NiftiMainUdpConn.send(message, 0, message.length, NiftiMainSendPort, NiftiMainIp );
        }
        else if ( RF_STOP_RECORD == 1)
        {
            // RF STOP
            console.log ( " SEND RF STOP RECORD COMMAND");
            var message = new Buffer( "{ \"tracking_number\": 1, \"slaveid\" : 0, \"command\": 65, \"params\" : { \"mode\": 0 } }" )
            NiftiMainUdpConn.send(message, 0, message.length, NiftiMainSendPort, NiftiMainIp );
        }
        else if ( RF_NODE_SHUTDOWN == 1)  
        {
            console.log ( " SEND RF NODE SHUTDOWN COMMAND");
            var message = new Buffer( "{ \"tracking_number\": 1, \"slaveid\" : 0, \"command\": 65, \"params\" : { \"mode\": 7 } }" )
            NiftiMainUdpConn.send(message, 0, message.length, NiftiMainSendPort, NiftiMainIp );
        }
        else if ( RF_SHUTDOWN == 1)  
        {
            console.log ( " SEND RF SHUTDOWN COMMAND");
            var message = new Buffer( "{ \"tracking_number\": 1, \"slaveid\" : 0, \"command\": 65, \"params\" : { \"mode\": 8 } }" )
            NiftiMainUdpConn.send(message, 0, message.length, NiftiMainSendPort, NiftiMainIp );
        }
        else if (RF_IDLE == 1 )
        {
            console.log ( " SEND RF IDLE COMMAND");
            var message = new Buffer( "{ \"tracking_number\": 1, \"slaveid\" : 0, \"command\": 65, \"params\" : { \"mode\": 6 } }" )
            NiftiMainUdpConn.send(message, 0, message.length, NiftiMainSendPort, NiftiMainIp );    
        }
        else
        {
            //nop();
        }
    }
    else if ( SEND_CONFIGURATION == 1 )
    {
        if ( prev_rf_ws_count + SEND_CONFIG_DELAY <= check_rf_ws_count )
        {
            var message = this.generateUDPJsonRequests( SET_NODE_CONFIG, config ); // Retried every 30 seconds.
            NiftiMainUdpConn.send(message, 0, message.length, NiftiMainSendPort, NiftiMainIp );
            prev_rf_ws_count = check_rf_ws_count + SEND_CONFIG_DELAY;
        }
        check_rf_ws_count = check_rf_ws_count + 1;
    }
}

// Process niftimian udp message response
process_niftmaster_udpmsg = function (msg)
{
   // console.log (" NODE APP RECEVIED" + msg.toString('utf8'));
    var slaveid  = 0;
    var command  = 0;
    var params   = 0;
    var json_obj = JSON.parse(msg.toString('utf8'));
    var message  =  msg.toString('utf8');
    for (var jkey in json_obj ) 
    {
        if ( jkey == "slaveid" )
        {
            slaveid = json_obj[jkey];
        }
        else if (jkey == "command" )
        {
            command = json_obj[jkey];
        }
        else if ( jkey == "result" )
        {
            params = json_obj[jkey];
        }
    }
    if ( command == SET_NODE_CONFIG )
    {
        for ( var jkey in params )
        {
            if ( jkey == "config")
            {
                if ( params[jkey] == 1 )
                {
                    console.log ( " NIFTI Node config sent successfully" );
                    SEND_CONFIGURATION = 0; // Config is sent successfully
                }
            }
        }
    }
    else if ( command == 65 )
    {
        for ( var jkey in params )
        {
            if ( jkey == "mode" )
            {
                var rf_mode = params[jkey];
                if ( rf_mode == 0 && slaveid == parseInt(slave_ngw05_SerialNumber,10) )
                {
                    RF_STOP_RECORD = 0;
                    console.log ( "NIFTI RF cards stopped recording" );
                    stitch_data();
                }
                else if ( rf_mode == 4 && slaveid == parseInt(slave_ngw05_SerialNumber,10) )
                {
                    RF_START_RECORD = 0;
                    console.log ( "NIFTI RF cards started recording ");
                }
                else if ( rf_mode == 6 && slaveid == parseInt(slave_ngw05_SerialNumber,10) )
                {
                    RF_IDLE = 0;
                    console.log ( "NIFTI gateway & nodes are idle" );
                }
                else if ( rf_mode == 7 && slaveid == parseInt(slave_ngw05_SerialNumber,10) )
                {
                    RF_NODE_SHUTDOWN = 0;
                    console.log ( "NIFTI nodes are Shutdown & Triggering gateway shutdown");
                    var message = new Buffer( "{ \"tracking_number\": 1, \"slaveid\" : 0, \"command\": 65, \"params\" : { \"mode\": 8 } }" )
                    NiftiMainUdpConn.send(message, 0, message.length, NiftiMasterSendPort, NiftiMasterIp );
                    RF_SHUTDOWN = 1;
                    // TURN OFF LEDS
                    const i2cset = spawn ( 'i2cset',['-y', 0, 0x18, 0x3, 0xff ] )
                    i2cset.stdout.on('data', (data) => {
                        console.log(`stdout: ${data}`);
                    });

                    i2cset.stderr.on('data', (data) => {
                        console.log(`stderr: ${data}`);
                    });

                    i2cset.on('close', (code) => {
                        console.log("child process exited with code ${code}" );
                    });
                    
                    console.log ("NIFTI nodes are Shutdown");
                }
                else if ( rf_mode == 8 && slaveid == parseInt(slave_ngw05_SerialNumber,10) )
                {
                    RF_SHUTDOWN = 0;
                    console.log ("NIFTI gateway & nodes are Shutdown");
                }
            }
        }
    }
    else if ( command == 4 )
    {
        for ( var jkey in params )
        {
            if ( jkey == "timestamp")
            {
                tstamp = params[jkey];
            }
            else if ( jkey == "uid")  
            {
                uid = params[jkey];
                // Insert the message in to niftiDB.
                for(var i = 0; i <  config.activeNodes.length; i++)
                {
                    if ( uid.toString() == config.activeNodes[i].nodeIdentifier )
                    {
                        const query = { 
                            text   : 'INSERT INTO streamout(data) VALUES($1)',
                            values : [message],
                        };
                        pool.query(query, (err, res) => {
                            if (err) 
                            {
                                console.log(err.stack)
                            } 
                        });
                    }
                }
            }
            else if ( jkey == "battery")
            {
                battery = params["battery"];
            }
            else if ( jkey == "rssi")
            {
                rssi = params["rssi"];
            }
            else if ( jkey == "data")
            {
                if (config == null)
                {
                    return;
                }
                var i = 0;
                var k = 0;
                var idx = 0;
                if ( params[jkey][0] != 0 && params[jkey][1] != 0 && params[jkey][2] != 0)
                {
                    for(var k = 0; k <  config.activeNodes.length; k++)
                    {
                        if ( uid.toString() == config.activeNodes[k].nodeIdentifier )
                        {
                            while ( i < 24 )
                            {
                                var valx = params[jkey][i];
                                var valy = params[jkey][i+1];
                                var valz = params[jkey][i+2];
                                if(config.activeNodes[k].channels[0].data.length < 10)
                                {
                                    config.activeNodes[k].channels[0].data.push((valx  * 10));
                                    config.activeNodes[k].channels[1].data.push((valy  * 10));
                                    config.activeNodes[k].channels[2].data.push((valz  * 10));
                                }
                                i += 3;
                            }
                        }
                    }
                }
            }
        }
    }
}

start = function(currentconfig)
{
    console.log('Starting broadcasting data');
    var val = "START\n";
    logNiftiEvents ( val );
    RF_START_RECORD     = 0;
    RF_STOP_RECORD      = 0;
    RF_NODE_SHUTDOWN    = 0; 
    RF_SHUTDOWN         = 0; 
    RF_IDLE             = 0;
    
    const query = { 
           text   : 'TRUNCATE streamout',
    };
    pool.query(query, (err, res) => {
        if (err) 
        {
            console.log(err.stack)
        } 
    });
    // send the current config to NIFTI main.
    SEND_CONFIGURATION = 1;
    check_rf_ws_count   = 0;
    prev_rf_ws_count    = 0;
    config = currentconfig;
    var message = generateUDPJsonRequests( SET_NODE_CONFIG, config );
    NiftiMainUdpConn.send(message, 0, message.length, NiftiMainSendPort, NiftiMainIp );
    
    
    testingindexer = 0; //this is only used to add variability to the test data. 
    //add the data element to each of the node channels in the current configuration
    for(i = 0; i <  config.activeNodes.length; i++)
    {
        config.activeNodes[i].channels[0].data = [];
        config.activeNodes[i].channels[0].maxValue = -999;
        config.activeNodes[i].channels[0].minValue = 999;
        config.activeNodes[i].channels[0].temperature = 0;
        config.activeNodes[i].channels[0].laststore = 0;
        config.activeNodes[i].channels[1].data = [];
        config.activeNodes[i].channels[1].maxValue = -999;
        config.activeNodes[i].channels[1].minValue = 999;
        config.activeNodes[i].channels[1].temperature = 0;
        config.activeNodes[i].channels[1].laststore = 0;
        config.activeNodes[i].channels[2].data = [];
        config.activeNodes[i].channels[2].maxValue = -999;
        config.activeNodes[i].channels[2].minValue = 999;
        config.activeNodes[i].channels[2].temperature = 0;
        config.activeNodes[i].channels[2].laststore = 0;
    }
    module.exports.config = config;
}

stop = function()
{
    console.log('Stoping broadcasting data')
    var val = "System idle mode\n";
    logNiftiEvents ( val );
    var message = new Buffer( "{ \"tracking_number\": 1, \"slaveid\" : 0, \"command\": 65, \"params\" : { \"mode\": 6 } }" )
    NiftiMainUdpConn.send(message, 0, message.length, NiftiMainSendPort, NiftiMainIp );
    RF_START_RECORD     = 0;
    RF_STOP_RECORD      = 0;
    RF_NODE_SHUTDOWN    = 0; 
    RF_SHUTDOWN         = 0; 
    RF_IDLE             = 1;
}

start_recording = function()
{
    console.log('Starting recording data')
    var val = "START RECORDING\n";
    logNiftiEvents ( val );
    var message = new Buffer( "{ \"tracking_number\": 1, \"slaveid\" : 0, \"command\": 65, \"params\" : { \"mode\": 4 } }" )
    NiftiMainUdpConn.send(message, 0, message.length, NiftiMainSendPort, NiftiMainIp );
    RF_START_RECORD     = 1;
    RF_STOP_RECORD      = 0;
    RF_NODE_SHUTDOWN    = 0; 
    RF_SHUTDOWN         = 0; 
    RF_IDLE             = 0;
}
    
stop_recording = function()
{
    console.log('Stoping recording data')
    var val = "STOP RECORDING\n";
    logNiftiEvents ( val );
    var message = new Buffer( "{ \"tracking_number\": 1, \"slaveid\" : 0, \"command\": 65, \"params\" : { \"mode\": 0 } }" )
    NiftiMainUdpConn.send(message, 0, message.length, NiftiMainSendPort, NiftiMainIp );
    RF_START_RECORD     = 0;
    RF_STOP_RECORD      = 1;
    RF_NODE_SHUTDOWN    = 0; 
    RF_SHUTDOWN         = 0; 
    RF_IDLE             = 0;
}

timestamp = function()
{
    console.log('Recording timestamp data')
    var val = "RECORDING TIMESTAMP\n";
    logNiftiEvents ( val );
}

shutdown = function()
{
    console.log("Shutting down the system")
    var val = "SHUTDOWN\n";
    logNiftiEvents ( val );
    RF_START_RECORD     = 0;
    RF_STOP_RECORD      = 0;
    RF_NODE_SHUTDOWN    = 1; 
    RF_SHUTDOWN         = 0; 
    RF_IDLE             = 0;
    var message = new Buffer( "{ \"tracking_number\": 1, \"slaveid\" : 0, \"command\": 65, \"params\" : { \"mode\": 7 } }" )
    NiftiMainUdpConn.send(message, 0, message.length, NiftiMainSendPort, NiftiMainIp );
}
   
module.exports.niftiudp_connect              = niftiudp_connect;
module.exports.pg_connect                    = pg_connect;
module.exports.logNiftiEvents                = logNiftiEvents;
module.exports.check_niftimain_requests      = check_niftimain_requests;
module.exports.process_niftmaster_udpmsg     = process_niftmaster_udpmsg;
module.exports.start                         = start;
module.exports.stop                          = stop;
module.exports.start_recording               = start_recording;
module.exports.stop_recording                = stop_recording;
module.exports.timestamp                     = timestamp;
module.exports.shutdown                      = shutdown;
module.exports.config                        = config;
module.exports.NiftiMainUdpConn              = NiftiMainUdpConn;
