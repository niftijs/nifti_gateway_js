/*!
*       \brief The etherCat engine is responsible for handing the parsing of messages to the linux etherCat subsystem that is in control of the EtherCat Drivers and Event Queue. The 
*               java script engine will send messages into the etherCat and also receive messages back from the etherCat subsystem. The Commands are Asyncronous. 
*/

function etherCatEngine() {
        //private values go in here
        var moduleversion = 1234;
        //the gateway object for the system. 
        var gateway = null;

      this.Initialise = function(){

      }

      this.getVersionID = function(){
          return moduleversion;
      }

      
      this.getGatewayObject = function()
      {
          return gateway;
      }


}; //close of function and define 

module.exports = etherCatEngine;