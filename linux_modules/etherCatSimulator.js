/*!
*       \brief The etherCat Simulator is responsible for pretending to be an etherCat setup in the system and running some background items in the system to update status messages in the system
*               and is only used for debug purposes with an external Application. 
*/

function etherCatSimulator(niftimodule,globals) {
        //private values go in here
        var moduleversion = 1234;
        var gateway = null;
        var niftimodule = niftimodule;
        var datastore = [];
        var global = globals
        var channeldatastore = [];

        console.log("Loaded EtherCat Simulator: Simulation Mode is Active. "+ moduleversion);

      this.Initialise = function()
      {
            //create a minimum viable simulator. 

            gateway = niftimodule.Nifti_CreateGatewayObject("GWSerialNumber","NGW-03","1.1.0","1.2.0",1,1);

            
            var PowerManagementSlave = niftimodule.Nifti_CreateSlave("NGW-02","powerSerialNumber","2.1.0","2.2.0");
            //add the capabillities
            var powersource = niftimodule.Nifti_CreateCap_PowerSource(28.4,0.5,0,3.30,0.500,5.01,0.800,23.5);
            PowerManagementSlave.Capabilities.push(powersource);
             niftimodule.Nifti_GatewayAddSlave(gateway,PowerManagementSlave);



            var BatteryModuleSlave = niftimodule.Nifti_CreateSlave("NGW-09","BatterySerialNumber","7.1.0","7.2.0");  
            //add the capabillities
            BatteryModuleSlave.Capabilities.push(niftimodule.Nifti_CreateCap_PowerIn(3.30,0.100,5.50,0.124,28.5));
            BatteryModuleSlave.Capabilities.push(niftimodule.Nifti_CreateCap_PowerStore(0,8000,7600));
           
            var NiftiLinkSlave = niftimodule.Nifti_CreateSlave("NGW-05","NiftiLinkSerialNumer","5.1.0","5.2.0");
            NiftiLinkSlave.Capabilities.push(niftimodule.Nifti_CreateCap_PowerIn(3.27,1.10,5.48,0.777,32.5));
            //add some nodes to the NiftiLinkSlave. 
            var channels = [1,2,3,4];
            var niftilink =  niftimodule.Nifti_CreateCap_NiftiLink(channels);
            niftilink.NodeList.push(niftimodule.Nifti_CreateAccelerometer("nodesn-0000",100,99,45,"idle",0));
            niftilink.NodeList.push(niftimodule.Nifti_CreateAccelerometer("nodesn-0001",99,98,44,"idle",0));
            niftilink.NodeList.push(niftimodule.Nifti_CreateAccelerometer("nodesn-0002",98,97,43,"idle",0));
            niftilink.NodeList.push(niftimodule.Nifti_CreateAccelerometer("nodesn-0003",97,96,42,"idle",0));

            NewDataNode("nodesn-0000");
            NewDataNode("nodesn-0001");
            NewDataNode("nodesn-0002");
            NewDataNode("nodesn-0003");


            NiftiLinkSlave.Capabilities.push(niftilink);

            //add the slaves to the gateway
           
            niftimodule.Nifti_GatewayAddSlave(gateway,BatteryModuleSlave);
            niftimodule.Nifti_GatewayAddSlave(gateway,NiftiLinkSlave);
            

      }

      this.getVersionID = function(){
          return moduleversion;
      }

      this.getGatewayObject = function()
      {
          return gateway;
      }
      /*!
      * \fn Runtime
      * \brief The Runtime funtion is responsible for updating the Simulator so that the application can see actual data I/O.
      */
      this.Runtime = function(){

            
                //lets update the slave data in the system. 
                if(gateway != null)
                {
                            //update from the global item the configuration if it has changed. 
                            

                            var idx = 0;
                            for(idx =0; idx < gateway.Slaves.length; idx++ )
                            {
                                    //udpate the power values, +- 0.1v. 
                                    if(gateway.Slaves[idx].SlaveType == "NGW-02")
                                    {
                                            //Power Management Slave. Lets update it. 
                                            var rand = Math.random() - 0.5;
                                            gateway.Slaves[idx].Capabilities[0].VoltageIn+=rand;
                                            if(gateway.Slaves[idx].Capabilities[0].VoltageIn > 29.0)
                                            {
                                                gateway.Slaves[idx].Capabilities[0].VoltageIn = 28.0;
                                            }
                                    }
                                    else if(gateway.Slaves[idx].SlaveType == "NGW-05")
                                    {
                                            //nifti link. Update the power in. 
                                             var rand = Math.random() - 0.5;
                                             gateway.Slaves[idx].Capabilities[0].VoltageIn3v3+=rand;
                                             gateway.Slaves[idx].Capabilities[0].VoltageIn5v0+=rand;
                                             
                                           //now udpate the nifti nodes. 
                                           var count =  gateway.Slaves[idx].Capabilities[1].NodeList.length;
                                          var d = new Date();
                                          var milliseconds = Math.round(d.getTime()) - 1502200000*1000; 

                                           var i = 0;
                                           for(i = 0; i < count; i++)
                                           {
                                                    //variable RSSI. 
                                                    gateway.Slaves[idx].Capabilities[1].NodeList[i].Rssi+=rand;
                                                    if( gateway.Slaves[idx].Capabilities[1].NodeList[i].Rssi > 100)
                                                    {
                                                         gateway.Slaves[idx].Capabilities[1].NodeList[i].Rssi = 100;
                                                    }
                                                    if( gateway.Slaves[idx].Capabilities[1].NodeList[i].Rssi < 0)
                                                    {
                                                         gateway.Slaves[idx].Capabilities[1].NodeList[i].Rssi = 0;
                                                    }
                                                    //in the storage database add 10 new records. 
                                                    datastore[i].Rssi = gateway.Slaves[idx].Capabilities[1].NodeList[i].Rssi;
                                                    datastore[i].Battery = gateway.Slaves[idx].Capabilities[1].NodeList[i].Battery;
                                                    datastore[i].RemainingBatteryTime = datastore[i].Battery * 120; 
                                                    datastore[i].Temperature = 25.5;
                                                    //update each of the sensors. 
                                                    var j = 0;
                                                    for(j = 0; j < datastore[i].Channels.length; j++)
                                                    {
                                                         datastore[i].Channels[j].value += rand;
                                                         newDataRecord(datastore[i].Channels[j],milliseconds,datastore[i].Channels[j].value);
                                                         if(datastore[i].Channels[j].value > datastore[i].Channels[j].maxval)
                                                         {
                                                             datastore[i].Channels[j].maxval = datastore[i].Channels[j].value;
                                                         }
                                                         if(datastore[i].Channels[j].value <  datastore[i].Channels[j].minval)
                                                         {
                                                              datastore[i].Channels[j].minval = datastore[i].Channels[j].value;
                                                         }
                                                         if(datastore[i].Channels[j].value >=  datastore[i].Channels[j].alarmmax)
                                                         {
                                                              datastore[i].Channels[j].alarm |= 0x01;
                                                         }
                                                         else
                                                         {
                                                               datastore[i].Channels[j].alarm &= ~0x01;
                                                         }

                                                         if(datastore[i].Channels[j].value <=  datastore[i].Channels[j].alarmmin)
                                                         {
                                                              datastore[i].Channels[j].alarm |= 0x02;
                                                         }
                                                         else
                                                         {
                                                               datastore[i].Channels[j].alarm &= ~0x02;
                                                         }


                                                    }
                                           }
                                    }
                                    else if(gateway.Slaves[idx].SlaveType == "NGW-09")
                                    {
                                            //update the power in. 
                                             var rand = Math.random() - 0.5;
                                             gateway.Slaves[idx].Capabilities[0].VoltageIn3v3+=rand;
                                             gateway.Slaves[idx].Capabilities[0].VoltageIn5v0+=rand;
                                            
                                            //update the battery
                                            //  cap.RequiredPower = requiredpower;
                                            //    cap.MaxCapacity = maxcapacity;
                                            //    cap.CurrentCapacity = currentcapacity;
                                             gateway.Slaves[idx].Capabilities[1].CurrentCapacity+=rand;
                                             if(gateway.Slaves[idx].Capabilities[1].CurrentCapacity > gateway.Slaves[idx].Capabilities[1].MaxCapacity)
                                             {
                                                    gateway.Slaves[idx].Capabilities[1].CurrentCapacity = gateway.Slaves[idx].Capabilities[1].MaxCapacity;
                                             }
                                             if(gateway.Slaves[idx].Capabilities[1].CurrentCapacity < 0 )
                                             {
                                                    gateway.Slaves[idx].Capabilities[1].CurrentCapacity = 0;
                                             }



                                    }


                            }

                }
            
      }

      //creates a new node to add data to in the system 
      function NewDataNode(serialnumber)
      {
            var node = new Object();
            node.SerialNumber = serialnumber;
            node.Rssi = 0;
            node.Battery = 0;
            node.RemainingBatteryTime = 0;
            node.Temperature = 0;
            node.Channels = [];

            NewDataChannel(node,"x");
            NewDataChannel(node,"y");
            NewDataChannel(node,"z");
            NewDataChannel(node,"TCore");
            NewDataChannel(node,"TSkin");


            datastore.push(node);
      }

      function NewDataChannel(node,name)
      {
            var channel = new Object();
            channel.name = name;
            channel.maxval = -9999;
            channel.minval = 9999;
            channel.alarmmax = 9999;
            channel.alarmmin = -9999;
            channel.value = 0;
            channel.alarm = 0;
            channel.status = 0;
            channel.enabled = 0;
            channel.datastore = [];
            node.Channels.push(channel);
      }

      function newDataRecord(channel,datetime,value)
      {
            var record = new Object();
            record.DateTime = datetime;
            record.value = value;
            //only save the data if the system is recording
            if(global.getSystemState() == 5)
            {
                  channel.datastore.push(record);
            }
      }

      function ResetAllDataChannels()
      {
            var count = datastore.length;

             var i = 0;
            for(i = 0; i < count; i++){
                  var j = 0;
                   for(j = 0; j < datastore[i].Channels.length; j++)
                  {
                        datastore[i].Channels[j].enabled = 0;
                  }

            } 
      }

      function SetDataChannel(nodeid,channel,enabled)
      {
            var count =  datastore.length;

            var i = 0;
            for(i = 0; i < count; i++){
                  if(datastore[i].SerialNumber == nodeid)
                  {
                        var j = 0;
                        for(j = 0; j < datastore[i].Channels.length; j++)
                        {
                              if( datastore[i].Channels[j].name == channel)
                              {
                                    datastore[i].Channels[j].enabled = enabled;
                                    return;
                              }
                        }
                      return;
                  }

            } 
      }

      /*!
      * \brie Sends the data store to the sender to send off if required. 
      *
      *
      */
      this.getNodeData = function()
      {
            //check to see if the configuration has changed. 
            if(global.getActiveConfigurationChanged())
            {
                  //set all nodes configurations to false. 
                  ResetAllDataChannels();
                  //get the configuration
                  var configuration = global.getActiveConfiguration();
                  //go through the active nodes. 
                  var count = configuration.activeNodes.length;
                  var idx = 0;
                  for(idx = 0; idx < count; idx++)
                  {     
                        SetDataChannel(configuration.activeNodes[idx].nodeIdentifier,configuration.activeNodes[idx].channels[0].name,configuration.activeNodes[idx].channels[0].enabled);
                        SetDataChannel(configuration.activeNodes[idx].nodeIdentifier,configuration.activeNodes[idx].channels[1].name,configuration.activeNodes[idx].channels[1].enabled);
                        SetDataChannel(configuration.activeNodes[idx].nodeIdentifier,configuration.activeNodes[idx].channels[2].name,configuration.activeNodes[idx].channels[2].enabled);
                  }
            }

          return datastore;
      }

}; //close of function and define 

module.exports = etherCatSimulator;